package main

import (
	"context"
	"fmt"
	"github.com/99designs/gqlgen/graphql/handler"
	_ "github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	_ "github.com/99designs/gqlgen/graphql/playground"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/go-chi/chi"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/rs/cors"
	_ "github.com/vektah/gqlparser/v2/gqlerror"
	"gitlab.com/tglobal/file-parser-service/config"
	"gitlab.com/tglobal/file-parser-service/graph"
	"gitlab.com/tglobal/file-parser-service/graph/generated"
	"gitlab.com/tglobal/file-parser-service/internal/repositories/factory"
	"gitlab.com/tglobal/file-parser-service/internal/services"
	"gitlab.com/tglobal/file-parser-service/internal/services/api"
	"gitlab.com/tglobal/file-parser-service/internal/services/factories"
	interfaces2 "gitlab.com/tglobal/file-parser-service/internal/services/interfaces"
	"log"
	"net/http"
)

var (
	buildVersion = "v1.0.0"
	buildDate    = "10.10.2022"
	buildCommit  = ""
)

var (
	sqsSvc *sqs.SQS
)

func main() {
	fmt.Printf("Build version:%s\n", buildVersion)
	fmt.Printf("Build date:%s\n", buildDate)
	fmt.Printf("Build commit:%s\n", buildCommit)

	configFromFile, err := config.LoadConfigFile("config.json")
	if err != nil {
		log.Fatalf("i can't load configuration file:" + err.Error())
	}
	cfg, err := config.GetConfigSettings(configFromFile)
	if err != nil {
		log.Fatalf("Can't read config: %s", err.Error())
	}

	var pool *pgxpool.Pool
	context := context.Background()
	pool, err = pgxpool.Connect(context, cfg.DatabaseDsn)
	if err != nil {
		log.Fatalf("Can't connect to the database server: %s", err.Error())
	}
	defer pool.Close()

	jRep := factory.NewJournalRepository(context, pool)
	jService := services.NewJournalService(context, jRep)

	fsApiService := api.NewFileStorageServiceApi(cfg.FileService, jService)
	tGenService := services.NewTokenGeneratorService(cfg.TokenGen)
	fStorage := services.NewFileStorageService(cfg.FileService, fsApiService, tGenService)

	userRep := factory.NewUserRepository(context, pool)
	fileRep := factory.NewFileRepository(context, pool)
	fileStructRep := factory.NewFileStructRepository(context, pool)
	filePackageRep := factory.NewFilePackageRepository(pool)

	fStructService := services.NewFileStructService(cfg.FileStructValidator, pool, fileStructRep, jService)

	authApi := api.NewAuthServiceApi(cfg.Auth, jService)
	authService := services.NewAuthServiceService(cfg.Auth, authApi)

	templService := services.NewTemplateService(cfg.HasuraTemplates)
	senderApi := api.NewHasuraServiceApi(cfg.Sender, jService)
	senderService := services.NewSenderProductService(cfg.Sender, senderApi, templService)

	sqsSvc = factories.NewSQS(cfg.AmazonSQS)
	prodSQSService := services.NewProducerSQSService(cfg.AmazonSQS, sqsSvc)
	fs := services.NewFileService(cfg, pool, userRep, fileRep, fileStructRep, filePackageRep, fStorage, fStructService,
		senderService, prodSQSService)

	consumerSQSService := services.NewConsumerSQSService(cfg.AmazonSQS, sqsSvc, fs, filePackageRep)
	chnMessages := make(chan *sqs.Message, 2)
	go consumerSQSService.RunPollSQSWorker(chnMessages)
	go consumerSQSService.MessageSQSHandler(context, chnMessages)

	runGraphQLServer(cfg, fs, authService, jService)
}

func runGraphQLServer(cfg config.Config, fs services.FileInterface, authService services.AuthServiceInterface, js interfaces2.JournalService) {
	router := chi.NewRouter()

	// Add CORS middleware around every request
	// See https://github.com/rs/cors for full option listing
	router.Use(cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedHeaders:   []string{"Origin", "X-Requested-With", "Content-Type", "Accept"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "OPTIONS"},
		AllowCredentials: false,
		Debug:            true,
	}).Handler)

	r := graph.NewRootResolvers(fs, js)
	es := generated.NewExecutableSchema(r)
	srv := handler.NewDefaultServer(es)
	rootHandler := authService.AuthMiddlewareHandler(srv)

	router.Handle("/", playground.Handler("GraphQL playground", "/query"))
	router.Handle("/query", rootHandler)

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", cfg.ServerAddress)
	log.Fatal(http.ListenAndServe(cfg.ServerAddress, router))
}

func CorsMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// allow cross domain AJAX requests
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
		w.Header().Set("Access-Control-Allow-Methods", "GET, POST,OPTIONS")
		if r.Method == "OPTIONS" {
			w.WriteHeader(http.StatusOK)
			return
		}
		next.ServeHTTP(w, r)
	})
}
