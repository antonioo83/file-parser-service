# File Parser Service
This service have been using for parse Excel document that contains products and send these products to the Hasura.

Algorithm of actions:
1. Frontend sends request to file parser service and get response with header structures;
2. Frontend sends request with additional parameters for columns to File parser service;
3. File parser service sends products to Hasura by Amazon SNS.

Therefore, as you can see, it is required that external services be available:
1. File service: https://tglobaldev.atlassian.net/wiki/spaces/BACK/pages/16089120/File+service
2. Auth service: https://tglobaldev.atlassian.net/wiki/spaces/BACK/pages/15794222/Authorization+service
3. Hasura market: https://tglobaldev.atlassian.net/wiki/spaces/BACK/pages/15958021/Hasura+Market

At the current version use:
1. PostgresSQL 15.3: https://www.postgresql.org
2. Golang 1.18: https://go.dev 
3. Amazon SNS: https://docs.aws.amazon.com/sdk-for-go/api/service/sns/

For more information go to link: https://tglobaldev.atlassian.net/wiki/spaces/BACK/pages/25231361/File+parser

## Getting started

1. Run migrations: ```migrate -database postgres://user:pass@host:5432/db-name?sslmode=disable -path migrations up```
For migration use library: https://github.com/golang-migrate/migrate so you should install this vendor.
2. Run tests: ``` go test file-parser-service/internal/server/ ```
3. Run service ``` go run file-parser-service/cmd/parser/main.go ```
 
## Getting started with docker compose
1. ```cd apps/file-parser-service```
2. ```git branch -M main```
3. ```git pull```
4. ```cp .env-prod .env```
5. ```docker compose up -d server --force-recreate --detach --build```

## Authors and acknowledgment
Developed by TGlobal team.

## License
It's commercial license.