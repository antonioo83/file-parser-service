# specify the base image to  be used for the application, alpine or ubuntu
FROM golang:1.18 as builder

ARG VERSION=unknowned

# create a working directory inside the image.
WORKDIR /app
# copy Go modules and dependencies to image.
COPY . .
# compile application.
RUN GO111MODULE=on GOOS=linux CGO_ENABLED=0 \
    go build -ldflags "-s -w -X main.version=${VERSION}" \
    -o /out cmd/parser/main.go
# specify the base image to  be used for the application, alpine or ubuntu.
FROM alpine:latest
RUN apk --no-cache add ca-certificates
# create a working directory inside the image.
WORKDIR /app
RUN mkdir -p /app/tmp
VOLUME /app/tmp
COPY ./templates /app/templates
COPY ./config.json /app/config.json
COPY ./RS512.key /app/RS512.key
COPY ./RS512.key.pub /app/RS512.key.pub
# copy Go application to image.
COPY --from=builder /out /bin/cmd

ENTRYPOINT ["/bin/cmd"]