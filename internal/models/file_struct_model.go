package models

import "time"

type FileStruct struct {
	ID int `copier:"-"`
	// FileID - id of the file model.
	FileID int `copier:"FileID"`
	// Code - column id (request: column.uuid) (duplicates "PropertyUID").
	Code string `copier:"Code"`
	// PropertyKey - property key of a product: column.propertyKey.
	PropertyKey string `copier:"PropertyKey"`
	// PropertyUID - property uid of a product: column.uuid.
	PropertyUID string `copier:"PropertyUID"`
	// CatalogCategoryKey - catalog of a product: column.catalogCategoryKey.
	CatalogCategoryKey string `copier:"CatalogCategoryKey"`
	// Label - label column of a document: column.label;
	Label string `copier:"Label"`
	// Type - data type of column (integer, string etc): column.type.
	Type string `copier:"Type"`
	// Position - number of column (counting starts from one): column.position.
	Position int `copier:"Position"`
	// Value - value cell of a document: column.value
	Value string `copier:"Value"`
	// Description - described product: column.description.
	Description string `copier:"Description"`
	// CreatedAt - date and time the resource was created
	CreatedAt time.Time
	// FileRelation - relation to the file model.
	FileRelation File
}
