package models

import "time"

const (
	// InProcess - pending status of the request.
	InProcess = 0
	// Sent - request was sent.
	Sent = 1
	// NotSent - request wasn't sent.
	NotSent = 3
)

// FilePackage - contains information about every request to Hasura shop.
type FilePackage struct {
	ID int `copier:"-"`
	// FileID - id of the file model.
	FileID int `copier:"FileID"`
	// MessageID - get from Amazon SQS.
	MessageID string `copier:"MessageID"`
	// StartIndex - start number row of Excel document.
	StartIndex int `copier:"StartIndex"`
	// FinishIndex - finish number row of Excel document.
	FinishIndex int `copier:"FinishIndex"`
	// Status - request status.
	Status int `copier:"Status"`
	// Message - response from Hasura.
	Message string `copier:"Message"`
	// UpdatedAt - create/update datetime.
	UpdatedAt time.Time
	// FileRelation - relation to the file model.
	FileRelation File
}
