package models

import "time"

const (
	GetTokenRequest              = "GET_TOKEN_FROM_AUTH_SERVICE_REQUEST"
	GetTokenResponse             = "GET_TOKEN_FROM_AUTH_SERVICE_RESPONSE"
	GetFileRequest               = "GET_FILE_FROM_FILE_SERVICE_REQUEST"
	GetFileResponse              = "GET_FILE_FROM_FILE_SERVICE_RESPONSE"
	GetFileStructRequest         = "GET_FILE_STRUCT_REQUEST"
	GetFileStructResponse        = "GET_FILE_STRUCT_RESPONSE"
	CreateUserFileStructRequest  = "CREATE_USER_FILE_STRUCT_REQUEST"
	CreateUserFileStructResponse = "CREATE_USER_FILE_STRUCT_RESPONSE"
	SendToHasuraRequest          = "SEND_TO_HASURA_REQUEST"
	SendToHasuraResponse         = "SEND_TO_HASURA_RESPONSE"
)

type Journal struct {
	ID int `copier:"-"`
	// FileID - id of the file model.
	FileID int `copier:"FileID"`
	// Title - name of a command (GetTokenRequest, SendToHasuraRequest with etc)
	Title string `copier:"Title"`
	// URL - resource where the message was sent.
	URL string `copier:"URL"`
	// Token - JWT token was used.
	Token string `copier:"Token"`
	// ResponseStatus - status of a resource.
	ResponseStatus int `copier:"ResponseStatus"`
	// ResponseContent - response from a resource.
	ResponseContent string `copier:"ResponseContent"`
	// Description - contains response string.
	Description string `copier:"Description"`
	// CreatedAt - date and time the resource was created
	CreatedAt time.Time
}
