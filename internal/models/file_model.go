package models

import "time"

// File - model contains detail information about a file;
type File struct {
	ID int `copier:"-"`
	// UserID - id of the user model.
	UserID int `copier:"UserID"`
	// Code - ID of a file in the Frontend resource (In current case Hasura shop).
	Code string `copier:"Code"`
	// CompanyUID - UID of the company in the Hasura shop.
	CompanyUID string `copier:"CompanyUID"`
	// Template - name of a template for generate request to Hasura shop.
	Template string `copier:"Template"`
	// Path - filepath to local storage.
	Path string `copier:"Path"`
	// Type - extension of a file.
	Type string `copier:"Type"`
	//TODO It's wrong name! rename to hash.
	// Cache - this is hash of filename.
	Cache string `copier:"Cache"`
	// Status - which shows the file processing status.
	Status string `copier:"Status"`
	// Message - about result.
	Message string `copier:"Message"`
	// Description - about file.
	Description string `copier:"Description"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   time.Time
}

// UpdateFileValue - using for change status of a file after every request.
type UpdateFileValue struct {
	// Status - which shows the file processing status.
	Status string
	// CompanyUID - UID of the company in the Hasura shop.
	CompanyUID string
	// Template - The name of the template being used.
	Template string
}
