package models

import "time"

type User struct {
	ID int `copier:"-"`
	// Code - ID from the external system, in the current case uuid from JWT token.
	Code string `copier:"Code"`
	//TODO: It's wrong name change name to payload.
	// Token - contains payload info of a JWT token.
	Token string `copier:"Token"`
	//TODO: It's wrong name add email property.
	// Title - contains email of an user.
	Title string `copier:"Title"`
	// Description - in the current case uuid from JWT token.
	Description string `copier:"Description"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   time.Time
}
