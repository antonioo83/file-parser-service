package utils

import (
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

func SendGetRequest(url string, queryParameters map[string]string, token string, timeout int) (status int, content string, err error) {
	return SendRequest(url, queryParameters, "GET", token, timeout, nil)
}

func SendPostRequest(url string, token string, timeout int, body io.Reader) (status int, content string, err error) {
	return SendRequest(url, nil, "POST", token, timeout, body)
}

func SendRequest(url string, queryParameters map[string]string, method string, token string, timeout int, body io.Reader) (status int, content string, err error) {
	req, err := GetRequest(url, queryParameters, method, body, token)
	client := &http.Client{
		CheckRedirect: func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		},
		Timeout: time.Second * time.Duration(timeout),
	}

	resp, err := client.Do(req)
	if err != nil {
		return 0, "", err
	}
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, "", err
	}
	defer ResourceClose(resp.Body)

	return resp.StatusCode, string(respBody), nil
}

func GetRequest(url string, queryParameters map[string]string, method string, body io.Reader, token string) (*http.Request, error) {
	req, err := http.NewRequest(method, url, body)
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+token)
	if queryParameters != nil {
		values := req.URL.Query()
		for param, value := range queryParameters {
			values.Add(param, value)
		}
		req.URL.RawQuery = values.Encode()
	}

	return req, err
}
