package postgres

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/tglobal/file-parser-service/internal/models"
	"gitlab.com/tglobal/file-parser-service/internal/repositories/interfaces"
)

type journalRepository struct {
	context    context.Context
	connection *pgxpool.Pool
}

func NewJournalRepository(context context.Context, pool *pgxpool.Pool) interfaces.JournalRepository {
	return &journalRepository{context, pool}
}

// Save creates a journal record in the database.
func (j journalRepository) Save(journal models.Journal) (int, error) {
	var lastInsertId int
	err := j.connection.QueryRow(
		j.context,
		`INSERT INTO 
               fp_journal (file_id, title, url, token, response_status, response_content, description)
               VALUES (NULLIF($1,0), $2, $3, $4, $5, $6, $7)  
             RETURNING id`,
		&journal.FileID, &journal.Title, &journal.URL, &journal.Token, &journal.ResponseStatus, &journal.ResponseContent,
		&journal.Description,
	).Scan(&lastInsertId)
	if err != nil {
		return 0, err
	}

	return lastInsertId, err
}
