package postgres

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/tglobal/file-parser-service/internal/models"
	"gitlab.com/tglobal/file-parser-service/internal/repositories/interfaces"
)

type fileRepository struct {
	context    context.Context
	connection *pgxpool.Pool
}

func NewFileRepository(context context.Context, pool *pgxpool.Pool) interfaces.FileRepository {
	return &fileRepository{context, pool}
}

// Save creates file record.
func (f fileRepository) Save(file models.File) (int, error) {
	var lastInsertId int
	err := f.connection.QueryRow(
		f.context,
		`INSERT INTO 
               fp_files (user_id, code, company_uid, path, type, cache, status, message, description, template)
               VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)  
             RETURNING id`,
		&file.UserID, &file.Code, &file.CompanyUID, &file.Path, &file.Type, &file.Cache,
		&file.Status, &file.Message, &file.Description, &file.Template,
	).Scan(&lastInsertId)
	if err != nil {
		return 0, err
	}

	return lastInsertId, err
}

// FindByCode gets model.
func (f fileRepository) FindByCode(userId int, code string) (*models.File, error) {
	var file models.File
	err := f.connection.QueryRow(
		f.context,
		`SELECT 
               id, user_id, code, company_uid, template, path, type, cache, status, message, description, created_at 
             FROM 
               fp_files 
             WHERE 
               user_id=$1 AND code=$2 AND deleted_at IS NULL`,
		userId, code,
	).Scan(&file.ID, &file.UserID, &file.Code, &file.CompanyUID, &file.Template, &file.Path, &file.Type, &file.Cache,
		&file.Status, &file.Message, &file.Description, &file.CreatedAt)
	if errors.Is(err, pgx.ErrNoRows) {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return &file, nil
}

// Update updates file record.
func (f fileRepository) Update(userId int, code string, update models.UpdateFileValue) error {
	_, err := f.connection.Query(
		f.context,
		`UPDATE 
               fp_files 
			 SET
			   status=$1,
			   company_uid=$2,
			   template=$3
             WHERE
               user_id=$4 AND code=$5`,
		update.Status,
		update.CompanyUID,
		update.Template,
		userId,
		code,
	)
	if err != nil {
		return err
	}

	return err
}
