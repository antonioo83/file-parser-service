package postgres

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/tglobal/file-parser-service/internal/models"
	"gitlab.com/tglobal/file-parser-service/internal/repositories/interfaces"
)

type userRepository struct {
	context    context.Context
	connection *pgxpool.Pool
}

func NewUserRepository(context context.Context, pool *pgxpool.Pool) interfaces.UserRepository {
	return &userRepository{context, pool}
}

// Save creates a model in the database.
func (u userRepository) Save(user models.User) (int, error) {
	var lastInsertId int
	err := u.connection.QueryRow(
		u.context,
		`INSERT INTO 
               fp_users (code, token, title, description)
               VALUES ($1, $2, $3, $4)  
             RETURNING id`,
		&user.Code, &user.Token, &user.Title, &user.Description,
	).Scan(&lastInsertId)
	if err != nil {
		return 0, err
	}

	return lastInsertId, err
}

// FindByCode gets a model by code from database.
func (u userRepository) FindByCode(code string) (*models.User, error) {
	var user models.User
	err := u.connection.QueryRow(
		u.context,
		`SELECT 
               id, code, token, title, description, created_at 
             FROM 
               fp_users 
             WHERE 
               code=$1 AND deleted_at IS NULL`,
		code,
	).Scan(&user.ID, &user.Code, &user.Token, &user.Title, &user.Description, &user.CreatedAt)
	if errors.Is(err, pgx.ErrNoRows) {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return &user, nil
}
