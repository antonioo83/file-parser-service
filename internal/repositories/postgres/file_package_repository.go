package postgres

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/tglobal/file-parser-service/internal/models"
	"gitlab.com/tglobal/file-parser-service/internal/repositories/interfaces"
)

type filePackageRepository struct {
	connection *pgxpool.Pool
}

func NewFilePackageRepository(pool *pgxpool.Pool) interfaces.FilePackageRepository {
	return &filePackageRepository{pool}
}

// MultipleInsert create multiple records in the package table.
func (f filePackageRepository) MultipleInsert(ctx context.Context, filePackages []models.FilePackage) error {
	b := &pgx.Batch{}
	for _, fp := range filePackages {
		b.Queue(
			`INSERT INTO 
                fp_file_packages (
					file_id, 
                    message_id,
					start_index, 
					finish_index, 
					status, 
					message
               )
               VALUES ($1, $2, $3, $4, $5, $6)`,
			fp.FileID, fp.MessageID, fp.StartIndex, fp.FinishIndex, fp.Status, fp.Message,
		)
	}
	r := f.connection.SendBatch(ctx, b)
	_, err := r.Exec()
	if err != nil {
		return err
	}

	return r.Close()
}

// UpdateStatus updates status and other attributes in the package table.
func (f filePackageRepository) UpdateStatus(ctx context.Context, fileId int, messageID string, status int,
	message string) error {
	_, err := f.connection.Exec(ctx,
		`UPDATE 
               fp_file_packages 
			 SET
			   status=$1,
			   message=$2
             WHERE
               file_id=$3 AND message_id=$4`,
		status, message, fileId, messageID,
	)
	if err != nil {
		return err
	}

	return err
}

// FindALL gets models from the package table by file id.
func (f filePackageRepository) FindALL(ctx context.Context, fileID int) (*[]models.FilePackage, error) {
	rows, err := f.connection.Query(
		ctx,
		`SELECT 
				fp."id", 
				fp."file_id", 
				fp."message_id", 
				fp."start_index", 
				fp."finish_index", 
				fp."status", 
				fp."message"
			FROM 
			    fp_file_packages fp
			WHERE 
  				fp."file_id"=$1 
			ORDER BY fp."id" ASC`,
		fileID,
	)
	if err != nil {
		return nil, err
	}

	models, err := getFilePackageModels(rows)
	if err != nil {
		return nil, err
	}

	return &models, nil
}

// getFilePackageModels returns array of the setting models.
func getFilePackageModels(rows pgx.Rows) ([]models.FilePackage, error) {
	var items []models.FilePackage
	var model models.FilePackage
	for rows.Next() {
		err := rows.Scan(
			&model.ID, &model.FileID, &model.MessageID, &model.StartIndex, &model.FinishIndex, &model.Status, &model.Message,
		)
		if err != nil {
			return nil, err
		}
		items = append(items, model)
	}

	return items, nil
}
