package postgres

import (
	"context"
	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/tglobal/file-parser-service/internal/models"
	"gitlab.com/tglobal/file-parser-service/internal/repositories/interfaces"
)

type fileStructRepository struct {
	context    context.Context
	connection *pgxpool.Pool
}

func NewFileStructRepository(context context.Context, pool *pgxpool.Pool) interfaces.FileStructRepository {
	return &fileStructRepository{context, pool}
}

// Save create file struct record (not used).
func (f fileStructRepository) Save(context context.Context, fileStruct models.FileStruct) (int, error) {
	var lastInsertId int
	err := f.connection.QueryRow(
		context,
		`INSERT INTO 
               fp_file_structures (
					"file_id", 
					"code", 
					"catalog_category_key", 
					"property_key", 
					"property_uid", 
					"label", 
                    "type", 
					"position", 
					"value", 
					"description"
               )
               VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, &10)  
             RETURNING id`,
		&fileStruct.FileID, &fileStruct.Code, &fileStruct.CatalogCategoryKey, &fileStruct.PropertyKey,
		&fileStruct.PropertyUID, &fileStruct.Label, &fileStruct.Type, &fileStruct.Position,
		&fileStruct.Value, &fileStruct.Description,
	).Scan(&lastInsertId)
	if err != nil {
		return 0, err
	}

	return lastInsertId, err
}

// MultipleInsert create multiple records in the  file struct table.
func (f fileStructRepository) MultipleInsert(fileStructs []models.FileStruct) error {
	b := &pgx.Batch{}
	for _, fs := range fileStructs {
		b.Queue(
			`INSERT INTO 
               fp_file_structures (
					"file_id", 
					"code", 
					"catalog_category_key", 
					"property_key",
					"property_uid",                                    
					"label", 
					"type", 
					"position", 
					"value",
					"description"
               )
               VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10)`,
			fs.FileID, fs.Code, fs.CatalogCategoryKey, fs.PropertyKey, fs.PropertyUID,
			fs.Label, fs.Type, fs.Position, fs.Value, fs.Description,
		)
	}
	r := f.connection.SendBatch(f.context, b)
	_, err := r.Exec()
	if err != nil {
		return err
	}

	return r.Close()
}

// FindALLByFileID gets array of the file struct items.
func (f fileStructRepository) FindALLByFileID(fileID int, key string) (*map[int]models.FileStruct, error) {
	rows, err := f.connection.Query(
		f.context,
		`SELECT 
				fs."id", 
				fs."file_id", 
				fs."code", 
				fs."catalog_category_key", 
				fs."property_key",
				fs."property_uid",                                    
				fs."label", 
				fs."type", 
				fs."position", 
				fs."value",
				fs."description",
				 fl."path",
				 fl."company_uid"
			FROM 
			    fp_file_structures fs
			LEFT JOIN fp_files fl ON fl."id"=fs."file_id"
			WHERE 
  				fs."file_id"=$1 
			ORDER BY fs."id" ASC`,
		fileID,
	)
	if err != nil {
		return nil, err
	}

	models, err := getFileStructModels(rows, key)
	if err != nil {
		return nil, err
	}

	return &models, nil
}

const (
	// PosKey gets models map will have key "pos".
	PosKey = "pos"
	// IdKey gets models map will have key "id" (not used).
	IdKey = "id"
)

// getFileStructModels returns array of the file struct models.
func getFileStructModels(rows pgx.Rows, key string) (map[int]models.FileStruct, error) {
	var items = make(map[int]models.FileStruct)
	var model models.FileStruct
	for rows.Next() {
		err := rows.Scan(
			&model.ID, &model.FileID, &model.Code, &model.CatalogCategoryKey, &model.PropertyKey, &model.PropertyUID,
			&model.Label, &model.Type, &model.Position, &model.Value, &model.Description,
			&model.FileRelation.Path, &model.FileRelation.CompanyUID,
		)
		if err != nil {
			return nil, err
		}
		if key == PosKey {
			items[model.Position] = model
		} else {
			items[model.ID] = model
		}
	}

	return items, nil
}
