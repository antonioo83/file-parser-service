package interfaces

import (
	"context"
	"gitlab.com/tglobal/file-parser-service/internal/models"
)

type FileStructRepository interface {
	// Save create file struct record (not used).
	Save(context context.Context, fileStruct models.FileStruct) (int, error)
	// MultipleInsert create multiple records in the  file struct table.
	MultipleInsert(fileStructs []models.FileStruct) error
	// FindALLByFileID gets  array of the file struct items.
	FindALLByFileID(fileID int, key string) (*map[int]models.FileStruct, error)
}
