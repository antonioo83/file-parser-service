package interfaces

import (
	"gitlab.com/tglobal/file-parser-service/internal/models"
)

type FileRepository interface {
	// Save creates file record.
	Save(file models.File) (int, error)
	// Update updates file record.
	Update(userId int, code string, update models.UpdateFileValue) error
	// FindByCode gets model.
	FindByCode(userId int, code string) (*models.File, error)
}
