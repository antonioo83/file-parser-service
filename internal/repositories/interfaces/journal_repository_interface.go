package interfaces

import (
	"gitlab.com/tglobal/file-parser-service/internal/models"
)

type JournalRepository interface {
	// Save - create journal record.
	Save(journal models.Journal) (int, error)
}
