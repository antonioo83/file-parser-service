package interfaces

import (
	"context"
	"gitlab.com/tglobal/file-parser-service/internal/models"
)

type FilePackageRepository interface {
	// MultipleInsert create multiple records in the package table.
	MultipleInsert(ctx context.Context, filePackages []models.FilePackage) error
	// FindALL gets models from the package table by file id.
	FindALL(ctx context.Context, fileID int) (*[]models.FilePackage, error)
	// UpdateStatus updates status and other attributes in thr package table.
	UpdateStatus(ctx context.Context, fileId int, messageID string, status int, message string) error
}
