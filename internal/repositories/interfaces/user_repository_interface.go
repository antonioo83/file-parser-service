package interfaces

import "gitlab.com/tglobal/file-parser-service/internal/models"

type UserRepository interface {
	// Save creates user.
	Save(user models.User) (int, error)
	// FindByCode - gets user by code value.
	FindByCode(code string) (*models.User, error)
}
