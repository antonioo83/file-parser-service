package factory

import (
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/tglobal/file-parser-service/internal/repositories/interfaces"
	"gitlab.com/tglobal/file-parser-service/internal/repositories/postgres"
)

func NewFilePackageRepository(pool *pgxpool.Pool) interfaces.FilePackageRepository {
	return postgres.NewFilePackageRepository(pool)
}
