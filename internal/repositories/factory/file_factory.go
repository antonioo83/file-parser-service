package factory

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/tglobal/file-parser-service/internal/repositories/interfaces"
	"gitlab.com/tglobal/file-parser-service/internal/repositories/postgres"
)

func NewFileRepository(context context.Context, pool *pgxpool.Pool) interfaces.FileRepository {
	return postgres.NewFileRepository(context, pool)
}
