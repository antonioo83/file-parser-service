package factory

import (
	"context"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/tglobal/file-parser-service/internal/repositories/interfaces"
	"gitlab.com/tglobal/file-parser-service/internal/repositories/postgres"
)

func NewFileStructRepository(context context.Context, pool *pgxpool.Pool) interfaces.FileStructRepository {
	return postgres.NewFileStructRepository(context, pool)
}
