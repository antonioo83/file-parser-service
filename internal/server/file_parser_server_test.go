package server

import (
	context2 "context"
	"encoding/base64"
	"errors"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/tglobal/file-parser-service/config"
	"gitlab.com/tglobal/file-parser-service/internal/repositories/factory"
	"gitlab.com/tglobal/file-parser-service/internal/services"
	"gitlab.com/tglobal/file-parser-service/internal/services/api"
	"golang.org/x/net/context"
	"log"
	"net/http"
	"os"
	"testing"
)

func TestFileParserRouters(t *testing.T) {
	configFromFile, err := config.LoadConfigFile("../../config.json")
	if err != nil {
		log.Fatalf("i can't load configuration file:" + err.Error())
	}
	cfg, err := config.GetConfigSettings(configFromFile)
	if err != nil {
		log.Fatalf("Can't read config: %s", err.Error())
	}

	var pool *pgxpool.Pool
	ctx := context2.Background()
	pool, err = pgxpool.Connect(ctx, cfg.DatabaseDsn)
	if err != nil {
		log.Fatalf("Can't connect to the database server: %s", err.Error())
	}
	defer pool.Close()

	jRep := factory.NewJournalRepository(ctx, pool)
	jService := services.NewJournalService(ctx, jRep)
	fs := GetFileService(ctx, cfg, pool, jService)
	authApi := api.NewAuthServiceApi(cfg.Auth, jService)
	authService := services.NewAuthServiceService(cfg.Auth, authApi)
	tokenGenService := services.NewTokenGeneratorService(cfg.TokenGen)
	fsApi := api.NewFileStorageServiceApi(cfg.FileService, jService)

	var srv http.Server
	go func() {
		srv = GetGraphQLServer(cfg, fs, authService, jService)
		log.Fatal(srv.ListenAndServe())
	}()

	jwtToken := runGenerateJWTToken(t, tokenGenService)
	fpApi := api.NewFileParserServiceApi("http://localhost:8081/query", jwtToken, cfg.Test.Timeout)
	for _, testFile := range cfg.Test.Files {
		authJWTToken := runGetAuthUserToken(ctx, t, cfg.Auth, authApi)
		filePath := runCreateFileInFS(ctx, t, testFile, fsApi, authJWTToken)
		runParseFile(ctx, t, testFile, filePath, fpApi)
		runCreateFileUserStructure(ctx, t, testFile, fpApi)
		runDeleteFileInFS(ctx, t, fsApi, jwtToken, filePath)
	}
	err = srv.Close()
	require.NoError(t, err)
}

func runGenerateJWTToken(t *testing.T, st services.TokenGeneratorServiceInterface) (jwtToken string) {
	jwtToken, err := st.GetTokenRS512()
	require.NoError(t, err)

	return jwtToken
}

func runGetAuthUserToken(ctx context.Context, t *testing.T, cfg config.Auth, as api.AuthServiceApiInterface) (authJWTToken string) {
	t.Run("Get user token from the \"Auth Service\"", func(t *testing.T) {
		var request = api.AccessTokenRequest{
			Login:    cfg.Login,
			Password: cfg.Password,
		}
		var err = errors.New("")
		authJWTToken, err = as.LoginUser(ctx, request)
		require.NoError(t, err)
	})

	return authJWTToken
}

func runCreateFileInFS(ctx context.Context, t *testing.T, testFile config.TestFile, fsApi api.FileStorageApiInterface,
	userToken string) (filepath string) {
	t.Run("Create a file in the \"File Service\"", func(t *testing.T) {
		data2, err := os.ReadFile(testFile.Filepath)
		require.NoError(t, err)
		content := base64.StdEncoding.EncodeToString(data2)

		var rp = make([]string, 0)
		var dp = make([]string, 0)
		var req = api.CreateFileRequest{
			Extension:         testFile.Extension,
			Name:              testFile.Name,
			Visibility:        testFile.Visibility,
			Folder:            testFile.Folder,
			ReadPermissions:   rp,
			DeletePermissions: dp,
			Content:           content,
		}
		filepath, err = fsApi.CreateFile(ctx, userToken, req)
		require.NoError(t, err)
	})

	return filepath
}

func runDeleteFileInFS(ctx context.Context, t *testing.T, fsApi api.FileStorageApiInterface, userToken string, filePath string) {
	t.Run("Delete a file from the \"File Service\"", func(t *testing.T) {
		err := fsApi.Delete(ctx, userToken, filePath)
		require.NoError(t, err)
	})
}

func runParseFile(ctx context.Context, t *testing.T, cfg config.TestFile, filePath string, fp api.FileParserServiceApiInterface) {
	t.Run("Send request to parse a file in the \"File Parser Service\"", func(t *testing.T) {
		var request = api.ParseFileRequest{
			Files: []api.FileItem{
				{
					FileUid: "28617a15-a6bb-46d2-934d-d2a9190b66e8",
					Path:    filePath,
					Type:    "excel",
				},
			},
		}
		content, err := fp.ParseFile(ctx, request)
		assert.NoError(t, err)
		assert.Equal(t, string(content), cfg.ParseFileResponse)
	})
}

func runCreateFileUserStructure(ctx context.Context, t *testing.T, cfg config.TestFile, fp api.FileParserServiceApiInterface) {
	t.Run("Send request for create user struct to the \"File Parser Service\"", func(t *testing.T) {
		content, err := fp.CreateFileUserStructure(ctx, cfg.CreateUserStructResponse)
		assert.NoError(t, err)
		assert.Equal(t, string(content), cfg.CreateUserStructResponse)
	})
}
