package server

import (
	"context"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/jackc/pgx/v4/pgxpool"
	_ "github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/tglobal/file-parser-service/config"
	"gitlab.com/tglobal/file-parser-service/graph"
	"gitlab.com/tglobal/file-parser-service/graph/generated"
	"gitlab.com/tglobal/file-parser-service/internal/repositories/factory"
	"gitlab.com/tglobal/file-parser-service/internal/services"
	"gitlab.com/tglobal/file-parser-service/internal/services/api"
	"gitlab.com/tglobal/file-parser-service/internal/services/factories"
	interfaces2 "gitlab.com/tglobal/file-parser-service/internal/services/interfaces"
	"log"
	"net/http"
)

func GetGraphQLServer(cfg config.Config, fs services.FileInterface, authService services.AuthServiceInterface, js interfaces2.JournalService) http.Server {
	r := graph.NewRootResolvers(fs, js)
	es := generated.NewExecutableSchema(r)
	srv := handler.NewDefaultServer(es)
	rootHandler := authService.AuthMiddlewareHandler(srv)

	http.Handle("/", playground.Handler("GraphQL playground", "/query"))
	http.Handle("/query", rootHandler)
	log.Printf("connect to http://localhost:%s/ for GraphQL playground", cfg.ServerAddress)

	server := &http.Server{
		Addr:    cfg.ServerAddress,
		Handler: nil,
	}

	return *server
}

func GetFileService(ctx context.Context, cfg config.Config, pool *pgxpool.Pool, js interfaces2.JournalService) services.FileInterface {
	fsApiService := api.NewFileStorageServiceApi(cfg.FileService, js)
	tGenService := services.NewTokenGeneratorService(cfg.TokenGen)
	fStorage := services.NewFileStorageService(cfg.FileService, fsApiService, tGenService)

	userRep := factory.NewUserRepository(ctx, pool)
	fileRep := factory.NewFileRepository(ctx, pool)
	fileStructRep := factory.NewFileStructRepository(ctx, pool)
	filePackageRep := factory.NewFilePackageRepository(pool)

	fStructService := services.NewFileStructService(cfg.FileStructValidator, pool, fileStructRep, js)

	templService := services.NewTemplateService(cfg.HasuraTemplates)
	senderApi := api.NewHasuraServiceApi(cfg.Sender, js)
	senderService := services.NewSenderProductService(cfg.Sender, senderApi, templService)

	sqsSvc := factories.NewSQS(cfg.AmazonSQS)
	prodSQSService := services.NewProducerSQSService(cfg.AmazonSQS, sqsSvc)
	fs := services.NewFileService(cfg, pool, userRep, fileRep, fileStructRep, filePackageRep, fStorage, fStructService,
		senderService, prodSQSService)

	return fs
}
