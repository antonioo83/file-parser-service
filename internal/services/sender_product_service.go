package services

import (
	"context"
	"fmt"
	"gitlab.com/tglobal/file-parser-service/config"
	"gitlab.com/tglobal/file-parser-service/internal/services/api"
)

type SenderProductServiceInterface interface {
	SendProducts(ctx context.Context, fileID int, template string, fileColumns map[int]*FileColumnResponse) error
}

type senderProductService struct {
	cfg         config.SenderService
	api         api.SenderServiceApiInterface
	tempService TemplateInterface
}

func NewSenderProductService(cfg config.SenderService, api api.SenderServiceApiInterface, tempService TemplateInterface) SenderProductServiceInterface {
	return &senderProductService{cfg, api, tempService}
}

// SendProducts - sends products to Hasura shop.
func (s senderProductService) SendProducts(ctx context.Context, fileID int, template string, fileColumns map[int]*FileColumnResponse) error {
	query, err := s.tempService.GetRequest(template, fileColumns)
	if err != nil {
		return fmt.Errorf("i can't create GraphQL request:%v", err)
	}

	return s.api.SendProducts(ctx, fileID, query)
}
