package services

import (
	"context"
	"encoding/base64"
	"fmt"
	_ "github.com/xuri/excelize/v2"
	"gitlab.com/tglobal/file-parser-service/config"
	"gitlab.com/tglobal/file-parser-service/internal/services/api"
	"os"
	"strings"
)

type FileStorageInterface interface {
	SaveFile(ctx context.Context, path string) (filePath string, err error)
}

type fileStorage struct {
	cfg          config.FileService
	api          api.FileStorageApiInterface
	tokenService TokenGeneratorServiceInterface
}

func NewFileStorageService(cfg config.FileService, api api.FileStorageApiInterface, tokenService TokenGeneratorServiceInterface) FileStorageInterface {
	return &fileStorage{cfg, api, tokenService}
}

type GetFileQuery struct {
	GetFile struct {
		Path string
	} `graphql:"getFile(path: $path)"`
}

// SaveFile - save file to the storage (using for tests).
func (f fileStorage) SaveFile(ctx context.Context, path string) (filePath string, err error) {
	token, err := f.tokenService.GetTokenRS512()
	if err != nil {
		return "", fmt.Errorf("i can't get a token: %w", err)
	}

	fileContent, err := f.api.GetFile(ctx, token, path)
	if err != nil {
		return "", fmt.Errorf("i can't get a file from the service: %w", err)
	}

	decodedContent, err := base64.StdEncoding.DecodeString(fileContent)
	if err != nil {
		return "", fmt.Errorf("i can't decode file content from base64: %w", err)
	}

	fpParts := strings.Split(path, "/")
	if len(fpParts) == 0 {
		return "", fmt.Errorf("i got wrong a filepath: %s", path)
	}

	filename := fpParts[len(fpParts)-1]
	filepath := f.cfg.TmpFilePath + filename
	file, err := os.Create(filepath)
	if err != nil {
		return "", fmt.Errorf("i can't create a file \"%s\": %w", filename, err)
	}
	defer func() {
		if err := file.Close(); err != nil {
			fmt.Printf("i can't close the file: %v", err)
		}
	}()

	_, err = file.WriteString(string(decodedContent))
	if err != nil {
		return "", fmt.Errorf("i can't write to the file \"%s\": %w", filename, err)
	}

	return filepath, nil
}
