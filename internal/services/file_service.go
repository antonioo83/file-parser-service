package services

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/jinzhu/copier"
	"gitlab.com/tglobal/file-parser-service/config"
	"gitlab.com/tglobal/file-parser-service/internal/models"
	"gitlab.com/tglobal/file-parser-service/internal/repositories/interfaces"
	"io"
	"os"
	"path/filepath"
)

const (
	// Document was created.
	statusOpen = "open"
	// Products of the document are ready to send to Hasura Shop.
	statusProcessed = "processed"
	// All products were sent to Hasura shop. (TODO: make it!)
	statusClose = "close"
)

type FileInterface interface {
	ParseFile(ctx context.Context, request FileRequest) (*FileStructResponse, error)
	CreateUserFileStruct(ctx context.Context, request UserFileStructRequest) (*UserFileStructResponse, error)
	SendFileProducts(ctx context.Context, message SendFileProductRequest) error
}

type fileService struct {
	cfg                config.Config
	connection         *pgxpool.Pool
	userRep            interfaces.UserRepository
	fileRep            interfaces.FileRepository
	fileStructRep      interfaces.FileStructRepository
	filePackageRep     interfaces.FilePackageRepository
	fs                 FileStorageInterface
	fStruct            FileStructServiceInterface
	senderService      SenderProductServiceInterface
	producerSQSService ProducerSQSServiceInterface
}

func NewFileService(cfg config.Config, connection *pgxpool.Pool, userRep interfaces.UserRepository, fileRep interfaces.FileRepository,
	fileStructRep interfaces.FileStructRepository, filePackageRep interfaces.FilePackageRepository, fs FileStorageInterface,
	fStruct FileStructServiceInterface, senderService SenderProductServiceInterface, producerSQSService ProducerSQSServiceInterface) FileInterface {
	return &fileService{cfg, connection, userRep, fileRep, fileStructRep,
		filePackageRep, fs, fStruct, senderService, producerSQSService}
}

// ParseFile gets request with path to the file and return parsed file header.
// If the document has been processed, the function will return the response that was received before.
func (f fileService) ParseFile(ctx context.Context, request FileRequest) (*FileStructResponse, error) {
	fPath, err := f.fs.SaveFile(ctx, request.Path)
	if err != nil {
		return nil, fmt.Errorf("i can't save file by filepath %s: %w", fPath, err)
	}

	response, err := f.fStruct.GetFileStructures(fPath)
	if err != nil {
		return nil, fmt.Errorf("can't get file struct: %w", err)
	}

	authUserCtx := ctx.Value(AuthUserCtxKey)
	authUser, ok := authUserCtx.(AuthUser)
	if !ok {
		return nil, fmt.Errorf("can't get user from the context")
	}
	if authUser.isError {
		return nil, fmt.Errorf("middleware got error: %s", authUser.ErrorMessage)
	}

	err = f.saveFileStructToDB(authUser, request.FileUID, fPath, response.Columns)
	if err != nil {
		return nil, fmt.Errorf("i can't save file struct in the database: %w", err)
	}

	return response, nil
}

// saveFileStructToDB saves parsed parameter about document column to the database.
func (f fileService) saveFileStructToDB(authUser AuthUser, FileUID string, fPath string, fColumns []ColumnsResponse) error {
	userId := 0
	user, err := f.userRep.FindByCode(authUser.Code)
	if err != nil {
		return fmt.Errorf("i can't find an user: %w", err)
	}
	if user == nil {
		var user = models.User{}
		err = copier.Copy(&user, &authUser)
		if err != nil {
			return fmt.Errorf("i can't copy auth to an user: %w", err)
		}
		userId, err = f.userRep.Save(user)
		if err != nil {
			return fmt.Errorf("i can't create an user in the database: %w", err)
		}
	} else {
		userId = user.ID
	}

	fileID := 0
	fileStatus := statusClose
	file, err := f.fileRep.FindByCode(userId, FileUID)
	if err != nil {
		return fmt.Errorf("i can't find a file: %w", err)
	}
	if file == nil {
		hashSum, err := f.getFileHash(fPath)
		if err != nil {
			return fmt.Errorf("i can't calc hash of the file: %w", err)
		}
		var newFile = models.File{
			UserID: userId,
			Code:   FileUID,
			Path:   fPath,
			Type:   filepath.Ext(fPath),
			Cache:  hashSum,
			Status: statusOpen,
		}
		fileStatus = newFile.Status
		fileID, err = f.fileRep.Save(newFile)
		if err != nil {
			return fmt.Errorf("i can'tsave the file: %w", err)
		}
	} else {
		fileID = file.ID
	}

	if fileStatus != statusOpen {
		return nil
	}

	var fsModels []models.FileStruct
	for _, col := range fColumns {
		var fsModel = models.FileStruct{}
		err = copier.Copy(&fsModel, &col)
		if err != nil {
			return fmt.Errorf("i can't copy response to the file struct: %w", err)
		}
		fsModel.FileID = fileID
		fsModels = append(fsModels, fsModel)
	}
	err = f.fileStructRep.MultipleInsert(fsModels)
	if err != nil {
		return fmt.Errorf("i can't insert the file structs to the database: %w", err)
	}

	return nil
}

// getFileHash generate hash by filepath. This hash doesn't use at the current moment and reserved for the next decisions.
func (f fileService) getFileHash(fPath string) (string, error) {
	file, err := os.Open(fPath)
	if err != nil {
		return "", fmt.Errorf("i can't open file: %s, error: %w", fPath, err)
	}
	defer file.Close()

	h := sha256.New()
	if _, err := io.Copy(h, file); err != nil {
		return "", fmt.Errorf("i can't copy data of the file: %s, error: %w", fPath, err)
	}

	return hex.EncodeToString(h.Sum(nil)), nil
}

const ResponseSuccessType = "success"
const ResponseSuccessCode = 0
const ResponseSuccessMess = "OK!"

type UserFileStructResponse struct {
	FileUID string `json:"fileUid"`
	Type    string `json:"type"`
	Code    int    `json:"code"`
	Message string `json:"message"`
}

// CreateUserFileStruct creates parameters for every column of document to database and send products to Hasura by Amazon SQS.
// If the document has processed/closed status, the function will return the error response.
func (f fileService) CreateUserFileStruct(ctx context.Context, request UserFileStructRequest) (*UserFileStructResponse, error) {
	authUserCtx := ctx.Value(AuthUserCtxKey)
	authUser, ok := authUserCtx.(AuthUser)
	if !ok {
		return nil, fmt.Errorf("can't get user from the context")
	}
	if authUser.isError {
		return nil, fmt.Errorf("middleware got error: %s", authUser.ErrorMessage)
	}

	user, err := f.userRep.FindByCode(authUser.Code)
	if err != nil {
		return nil, fmt.Errorf("i can't find an user: %w", err)
	}
	if user == nil {
		return nil, fmt.Errorf("i can't find an user in the database: %w", err)
	}
	file, err := f.fileRep.FindByCode(user.ID, request.FileUID)
	if err != nil {
		return nil, fmt.Errorf("i can't find a file: %w", err)
	}
	if file == nil {
		return nil, fmt.Errorf("i can't find a file in the database: %w", err)
	}
	if file.Status != statusOpen {
		return nil, fmt.Errorf("this file was processed before and have status: %s", file.Status)
	}

	var fsModels []models.FileStruct
	for _, col := range request.Columns {
		var fsModel = models.FileStruct{}
		err = copier.Copy(&fsModel, &col)
		if err != nil {
			return nil, fmt.Errorf("i can't copy response to the file struct: %w", err)
		}
		fsModel.FileID = file.ID
		fsModel.Code = col.Code
		fsModel.PropertyUID = col.Code
		fsModels = append(fsModels, fsModel)
	}
	err = f.fileStructRep.MultipleInsert(fsModels)
	if err != nil {
		return nil, fmt.Errorf("i can't insert the file structs to the database: %w", err)
	}

	ufValue := models.UpdateFileValue{
		Status:     statusProcessed,
		CompanyUID: request.CompanyUID,
		Template:   request.Template,
	}
	err = f.fileRep.Update(user.ID, file.Code, ufValue)
	if err != nil {
		return nil, fmt.Errorf("i can't status of the file in the database: %w", err)
	}

	rowCount, err := f.fStruct.GetFileRowCount(file.Path)
	if err != nil {
		return nil, fmt.Errorf("i can't get rows count from the file \"%s\": %w", file.Path, err)
	}
	if rowCount == 0 {
		return nil, fmt.Errorf("this file doesn't have rows: %s", file.Path)
	}
	if rowCount == 1 {
		return nil, fmt.Errorf("this file contains only header rows: %s", file.Path)
	}
	rowCount--

	startNumber := 1
	finishNumber := startNumber + f.cfg.Sender.ItemCount - 1
	var packageModels []models.FilePackage
	if finishNumber >= rowCount {
		var packageModel = models.FilePackage{
			FileID:      file.ID,
			StartIndex:  startNumber,
			FinishIndex: finishNumber,
			Status:      models.InProcess,
		}
		packageModels = append(packageModels, packageModel)
	}
	for finishNumber < rowCount {
		var packageModel = models.FilePackage{
			FileID:      file.ID,
			StartIndex:  startNumber,
			FinishIndex: finishNumber,
			Status:      models.InProcess,
		}
		packageModels = append(packageModels, packageModel)
		if finishNumber+f.cfg.Sender.ItemCount-1 >= rowCount {
			packageModel.StartIndex = finishNumber + 1
			packageModel.FinishIndex = rowCount
			packageModels = append(packageModels, packageModel)
			break
		} else {
			startNumber += f.cfg.Sender.ItemCount
			finishNumber += f.cfg.Sender.ItemCount
		}
	}

	for key, pModel := range packageModels {
		var mess = FileSQSMessage{
			FileID:       file.ID,
			FilePath:     file.Path,
			Template:     request.Template,
			StartNumber:  pModel.StartIndex,
			FinishNumber: pModel.FinishIndex,
		}
		//REVIEW ! Anton: you should write error and continue working.
		messID, err := f.producerSQSService.CreateSQSMessage(mess)
		if err != nil {
			return nil, fmt.Errorf("i can't produce message to qoueue : %w", err)
		}
		packageModels[key].MessageID = *messID
	}

	err = f.filePackageRep.MultipleInsert(ctx, packageModels)
	if err != nil {
		return nil, fmt.Errorf("i can't create package models for the file \"%s\": %w", file.Path, err)
	}

	var response UserFileStructResponse
	response.FileUID = file.Code
	response.Type = ResponseSuccessType
	response.Code = ResponseSuccessCode
	response.Message = ResponseSuccessMess

	return &response, nil
}

// SendFileProducts - sends products to Hasura shop by Amazon SQS.
func (f fileService) SendFileProducts(ctx context.Context, message SendFileProductRequest) error {
	fileColumns, err := f.fStruct.GetFileColumns(message)
	if err != nil {
		return fmt.Errorf("i can't get products from the file \"%s\": %w", message.FilePath, err)
	}

	err = f.senderService.SendProducts(ctx, message.FileID, message.Template, fileColumns)
	if err != nil {
		return fmt.Errorf("i can't send products of the file \"%s\" to the Hasura Market: %w", message.FilePath, err)
	}

	return nil
}
