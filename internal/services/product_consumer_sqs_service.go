package services

import (
	"context"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"gitlab.com/tglobal/file-parser-service/config"
	"gitlab.com/tglobal/file-parser-service/internal/models"
	"gitlab.com/tglobal/file-parser-service/internal/repositories/interfaces"
	"strconv"
)

type ConsumerSQSServiceInterface interface {
	RunPollSQSWorker(chnMessages chan<- *sqs.Message)
	MessageSQSHandler(ctx context.Context, chnMessages chan *sqs.Message)
}

type consumerSQSService struct {
	Config      config.AmazonSQS
	sqsSvc      *sqs.SQS
	fileService FileInterface
	fpRep       interfaces.FilePackageRepository
}

func NewConsumerSQSService(cfg config.AmazonSQS, sqsSvc *sqs.SQS, fileService FileInterface,
	filePackageRep interfaces.FilePackageRepository) ConsumerSQSServiceInterface {
	return &consumerSQSService{cfg, sqsSvc, fileService, filePackageRep}
}

// RunPollSQSWorker - polling Amazon SQS queue and write to the chanel got message.
func (p consumerSQSService) RunPollSQSWorker(chnMessages chan<- *sqs.Message) {
	for {
		result, err := p.sqsSvc.ReceiveMessage(&sqs.ReceiveMessageInput{
			AttributeNames: []*string{
				aws.String(sqs.MessageSystemAttributeNameSentTimestamp),
			},
			MessageAttributeNames: []*string{
				aws.String(sqs.QueueAttributeNameAll),
			},
			QueueUrl:            &p.Config.Receiver.QueueUrl,
			MaxNumberOfMessages: aws.Int64(p.Config.Receiver.MaxNumberOfMessages),
			VisibilityTimeout:   aws.Int64(p.Config.Receiver.VisibilityTimeout),
			WaitTimeSeconds:     aws.Int64(p.Config.Receiver.WaitTimeSeconds),
		})
		if err != nil {
			fmt.Printf("can't receive message: %w", err)
			return
		}

		for _, message := range result.Messages {
			chnMessages <- message
		}
	}
}

// MessageSQSHandler - gets message from the chanel and send request with products to the Hasura.
func (p consumerSQSService) MessageSQSHandler(ctx context.Context, chnMessages chan *sqs.Message) {
	for message := range chnMessages {
		fileID, err := strconv.Atoi(*message.MessageAttributes["FileID"].StringValue)
		if err != nil {
			fmt.Printf("can't get \"FileID\" value, message: %s, error: %s", *message.MessageId, err.Error())
		}
		filePath := *message.MessageAttributes["FilePath"].StringValue
		template := *message.MessageAttributes["Template"].StringValue
		startNumber, err := strconv.Atoi(*message.MessageAttributes["StartNumber"].StringValue)
		if err != nil {
			fmt.Printf("can't get \"StartNumber\" value, message: %s, error: %s", *message.MessageId, err.Error())
		}
		itemCount, err := strconv.Atoi(*message.MessageAttributes["FinishNumber"].StringValue)
		if err != nil {
			fmt.Printf("can't get \"FinishNumber\" value, message: %s, error: %s", *message.MessageId, err.Error())
		}

		//REVIEW ! Anton: remove message in 3 attempts.
		_, err = p.sqsSvc.DeleteMessage(&sqs.DeleteMessageInput{
			QueueUrl:      &p.Config.Receiver.QueueUrl,
			ReceiptHandle: message.ReceiptHandle,
		})
		if err != nil {
			fmt.Printf("can't delete SQS message: %s, error: %s", *message.MessageId, err.Error())
		}

		resultMessage := ""
		resultStatus := models.NotSent
		var req = SendFileProductRequest{
			FileID:       fileID,
			FilePath:     filePath,
			Template:     template,
			StartNumber:  startNumber,
			FinishNumber: itemCount,
		}
		err = p.fileService.SendFileProducts(ctx, req)
		if err != nil {
			resultMessage = "The message wasn't sent:" + err.Error()
		} else {
			resultStatus = models.Sent
			resultMessage = "OK!"
			fmt.Printf("the message was sent: %s\n", *message.MessageId)
		}
		err = p.fpRep.UpdateStatus(ctx, req.FileID, *message.MessageId, resultStatus, resultMessage)
		if err != nil {
			fmt.Printf("i can't update status of the SQS message: %s, error: %s\n", *message.MessageId, err.Error())
		}
	}
}
