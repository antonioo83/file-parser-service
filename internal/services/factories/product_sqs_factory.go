package factories

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sqs"
	"gitlab.com/tglobal/file-parser-service/config"
)

func NewSQS(cfg config.AmazonSQS) (sqsSvc *sqs.SQS) {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
		Config: aws.Config{
			Region: aws.String(cfg.Region),
			Credentials: credentials.NewStaticCredentials(
				cfg.Credential.ID,
				cfg.Credential.Secret,
				"",
			),
		},
	}))
	return sqs.New(sess,
		&aws.Config{
			Endpoint: &cfg.EndpointURL,
			Region:   aws.String(cfg.Region),
			Credentials: credentials.NewStaticCredentials(
				cfg.Credential.ID,
				cfg.Credential.Secret,
				"",
			),
		})
}
