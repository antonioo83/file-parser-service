package services

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
	"gitlab.com/tglobal/file-parser-service/config"
	"strconv"
)

type ProducerSQSServiceInterface interface {
	CreateSQSMessage(message FileSQSMessage) (*string, error)
}

type producerSQSService struct {
	Config config.AmazonSQS
	SqsSvc *sqs.SQS
}

func NewProducerSQSService(cfg config.AmazonSQS, sqsSvc *sqs.SQS) ProducerSQSServiceInterface {
	return &producerSQSService{cfg, sqsSvc}
}

type FileSQSMessage struct {
	FileID       int
	FilePath     string
	Template     string
	StartNumber  int
	FinishNumber int
}

// CreateSQSMessage - send message with product request to Amazon SQS.
func (p producerSQSService) CreateSQSMessage(message FileSQSMessage) (*string, error) {
	out, err := p.SqsSvc.SendMessage(&sqs.SendMessageInput{
		DelaySeconds: aws.Int64(p.Config.Sender.DelaySeconds),
		MessageAttributes: map[string]*sqs.MessageAttributeValue{
			"FileID": {
				DataType:    aws.String("Number"),
				StringValue: aws.String(strconv.Itoa(message.FileID)),
			},
			"FilePath": {
				DataType:    aws.String("String"),
				StringValue: aws.String(message.FilePath),
			},
			"Template": {
				DataType:    aws.String("String"),
				StringValue: aws.String(message.Template),
			},
			"StartNumber": {
				DataType:    aws.String("Number"),
				StringValue: aws.String(strconv.Itoa(message.StartNumber)),
			},
			"FinishNumber": {
				DataType:    aws.String("Number"),
				StringValue: aws.String(strconv.Itoa(message.FinishNumber)),
			},
		},
		MessageBody: aws.String("Information about current NY Times fiction bestseller for week of 12/11/2016."),
		QueueUrl:    &p.Config.Receiver.QueueUrl,
	})

	return out.MessageId, err
}
