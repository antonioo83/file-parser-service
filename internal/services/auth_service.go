// Package services this service allows to authorize user.
package services

import (
	"context"
	"fmt"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/dgrijalva/jwt-go"
	"gitlab.com/tglobal/file-parser-service/config"
	"gitlab.com/tglobal/file-parser-service/internal/services/api"
	"net/http"
	"os"
	"strings"
)

type AuthUser struct {
	// Code - user id in the external system.
	Code string
	// Title - user name, here Email.
	Title string
	// Token - JWT token.
	Token string
	// Description - additional information from JWT token.
	Description string
	// isError - error authorization.
	isError bool
	// ErrorMessage - error message.
	ErrorMessage string
}

type contextKey string

var (
	AuthUserCtxKey = contextKey("authUser")
)

type AuthServiceInterface interface {
	AuthMiddlewareHandler(srv *handler.Server) http.Handler
}

type authService struct {
	cfg config.Auth
	api api.AuthServiceApiInterface
}

func NewAuthServiceService(cfg config.Auth, api api.AuthServiceApiInterface) AuthServiceInterface {
	return &authService{cfg, api}
}

// AuthMiddlewareHandler - authorizes user by HTTP header of a request.
func (a authService) AuthMiddlewareHandler(srv *handler.Server) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		AuthHeader := r.Header.Get("Authorization")
		jwtToken := strings.Replace(AuthHeader, "Bearer", "", -1)
		jwtToken = strings.Replace(jwtToken, " ", "", -1)
		if jwtToken == "" {
			var authUser = AuthUser{
				isError:      true,
				ErrorMessage: "Error jwt token: token not found",
			}
			ctx := context.WithValue(r.Context(), AuthUserCtxKey, authUser)
			srv.ServeHTTP(w, r.WithContext(ctx))
		} else {
			claims := jwt.MapClaims{}
			_, err := jwt.ParseWithClaims(jwtToken, &claims, func(token *jwt.Token) (interface{}, error) {
				privateKey, err := os.ReadFile(a.cfg.PrivateKeyPath)
				if err != nil {
					return nil, fmt.Errorf("i can't read the jwt private key: %w", err)
				}
				parsedPrivateKey, err := jwt.ParseRSAPrivateKeyFromPEM(privateKey)
				if err != nil {
					return nil, fmt.Errorf("i can't parse the jwt private key: %w", err)
				}

				parts := strings.Split(jwtToken, ".")
				if len(parts) < 3 {
					return nil, fmt.Errorf("incorrect jwt token: %w", err)
				}

				method := jwt.GetSigningMethod(a.cfg.SigningMethod)
				sig, err := method.Sign(strings.Join(parts[0:2], "."), parsedPrivateKey)
				if err != nil {
					return nil, fmt.Errorf("error signing token: %w", err)
				}
				if sig != parts[2] {
					return nil, fmt.Errorf("incorrect signature")
				}

				pubKey, err := os.ReadFile(a.cfg.PublicKeyPath)
				parsedPublicKey, err := jwt.ParseRSAPublicKeyFromPEM(pubKey)
				if err != nil {
					return nil, fmt.Errorf("i can't read the jwt public key: %w", err)
				}

				return parsedPublicKey, nil
			})
			if err != nil && err.(*jwt.ValidationError).Errors != jwt.ValidationErrorExpired {
				var AuthUser = AuthUser{
					isError:      true,
					ErrorMessage: "Error signing token: " + err.Error(),
				}
				ctx := context.WithValue(r.Context(), AuthUserCtxKey, AuthUser)
				srv.ServeHTTP(w, r.WithContext(ctx))
			} else {
				isError := false
				errorMessage := ""
				uuid := ""
				if _, ok := claims["uuid"]; !ok {
					isError = true
					errorMessage = "jwt token don't contain uuid value"
				} else {
					uuid = claims["uuid"].(string)
				}

				payload := ""
				if _, ok := claims["payload"]; !ok {
					isError = true
					errorMessage = "jwt token don't contain payload value"
				} else {
					payload = claims["payload"].(string)
				}

				email := ""
				if _, ok := claims["email"]; !ok {
					isError = true
					errorMessage = "jwt token don't contain email value"
				} else {
					email = claims["email"].(string)
				}

				var authUser = AuthUser{
					Code:         uuid,
					Title:        email,
					Token:        payload,
					Description:  uuid,
					isError:      isError,
					ErrorMessage: errorMessage,
				}
				ctx := context.WithValue(r.Context(), AuthUserCtxKey, authUser)
				srv.ServeHTTP(w, r.WithContext(ctx))
			}
		}
	})
}
