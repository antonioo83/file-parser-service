// Package api this file implements API functions for collaboration with external File Service.
// For more information go to link: https://tglobaldev.atlassian.net/wiki/spaces/BACK/pages/16089120/File+service
package api

import (
	"context"
	"fmt"
	"github.com/hasura/go-graphql-client"
	"gitlab.com/tglobal/file-parser-service/config"
	"gitlab.com/tglobal/file-parser-service/internal/models"
	interfaces2 "gitlab.com/tglobal/file-parser-service/internal/services/interfaces"
	"golang.org/x/oauth2"
	"net/http"
	"time"
)

type FileStorageApiInterface interface {
	CreateFile(ctx context.Context, token string, req CreateFileRequest) (filepath string, err error)
	Delete(ctx context.Context, token string, path string) error
	GetFiles(ctx context.Context, folder string) error
	GetFile(ctx context.Context, token string, path string) (filePath string, err error)
	GetPublicFile(ctx context.Context, path string) error
}

type fileStorageApi struct {
	cfg config.FileService
	j   interfaces2.JournalService
}

func NewFileStorageServiceApi(cfg config.FileService, jService interfaces2.JournalService) FileStorageApiInterface {
	return &fileStorageApi{cfg, jService}
}

var gfs struct {
	GetFile struct {
		File struct {
			Content  string `graphql:"content"`
			Metadata struct {
				Extension string `graphql:"extension"`
				Name      string `graphql:"name"`
				CreatedBy string `graphql:"created_by"`
				DeletedAt string `graphql:"deleted_at"`
				DeletedBy string `graphql:"deleted_by"`
				Folder    string `graphql:"folder"`
			}
		} `graphql:"... on File"`
		Error struct {
			Message    string `graphql:"message"`
			StatusCode int    `graphql:"status_code"`
		} `graphql:"... on Error"`
	} `graphql:"getFile(path: $path)"`
}

// GetFile - gets filepath to uploaded to storage file.
func (f fileStorageApi) GetFile(ctx context.Context, token string, path string) (filePath string, err error) {
	httpClient := oauth2.NewClient(context.Background(), nil)
	httpClient.Timeout = time.Second * time.Duration(f.cfg.Timeout)
	client := graphql.NewClient(f.cfg.URL, httpClient)
	var rm = graphql.RequestModifier(func(r *http.Request) {
		r.Header.Add("Authorization", "Bearer "+token)
	})
	client = client.WithRequestModifier(rm)
	variables := map[string]interface{}{"path": path}

	var jModel = models.Journal{
		URL:   f.cfg.URL,
		Token: token,
		Title: models.GetFileRequest,
	}
	f.j.SaveRequestToJournal(jModel, variables)
	jModel.Title = models.GetFileResponse

	err = client.Query(ctx, &gfs, variables)
	if err != nil {
		f.j.SaveErrorResponseToJournal(jModel, err.Error())
		return "", fmt.Errorf("i can't send graphQL query to file storage service: %w", err)
	}
	if gfs.GetFile.Error.StatusCode != 0 {
		f.j.SaveResponseToJournal(jModel, gfs)
		return "", fmt.Errorf(
			"i got error from \"file service\", code: %d, message: %s",
			gfs.GetFile.Error.StatusCode,
			gfs.GetFile.Error.Message,
		)
	}

	jModel.Description = "OK!"
	f.j.SaveResponseToJournal(jModel, nil)

	return gfs.GetFile.File.Content, nil
}

var sfs struct {
	StoreFile struct {
		Message struct {
			Message string `graphql:"message"`
		} `graphql:"... on Message"`
	} `graphql:"storeFile(input:{extension: $extension, name: $name, content: $content, read_permissions: $read_permissions, delete_permissions: $delete_permissions, visibility: $visibility, folder: $folder})"`
}

type ModelInstance struct {
	Values string `json:"Values"`
}

type Extension struct {
	Values string `json:"Values"`
}

type Visibility struct {
	Values string `json:"Values"`
}

type Model struct {
	UUID     string        `json:"uuid"`
	Instance ModelInstance `json:"instance"`
}

type CreateFileRequest struct {
	Extension         string   `json:"extension"`
	Name              string   `json:"name"`
	Visibility        string   `json:"visibility"`
	Folder            string   `json:"folder"`
	ReadPermissions   []string `json:"read_permissions"`
	DeletePermissions []string `json:"delete_permissions"`
	Content           string   `json:"content"`
}

// CreateFile - saves file to the file storage.
func (f fileStorageApi) CreateFile(ctx context.Context, token string, req CreateFileRequest) (filepath string, err error) {
	httpClient := oauth2.NewClient(context.Background(), nil)
	httpClient.Timeout = time.Second * time.Duration(f.cfg.Timeout)
	client := graphql.NewClient(f.cfg.URL, httpClient)
	var rm = graphql.RequestModifier(func(r *http.Request) {
		r.Header.Add("Authorization", "Bearer "+token)
	})
	client = client.WithRequestModifier(rm)
	variables := map[string]interface{}{
		"extension":          req.Extension,
		"name":               req.Name,
		"content":            req.Content,
		"read_permissions":   req.ReadPermissions,
		"delete_permissions": req.DeletePermissions,
		"visibility":         req.Visibility,
		"folder":             req.Folder,
	}
	query := `mutation StoreFile($content:String!$delete_permissions:[Model!]!$extension:Extension!$folder:String!$name:String!
                  $read_permissions:[Model!]!$visibility:Visibility!){
                  storeFile(input:{
                      extension: $extension, 
                      name: $name, 
                      content: $content, 
                      read_permissions: $read_permissions, 
                      delete_permissions: $delete_permissions, 
                      visibility: $visibility, 
                      folder: $folder}){
                          ... on Message{
                              message
                          }
                      }
                  }`
	if err := client.Exec(ctx, query, &sfs, variables); err != nil {
		return "", fmt.Errorf("i can't send graphQL mutation to file storage service: %w", err)
	}

	return sfs.StoreFile.Message.Message, nil
}

var dfs struct {
	DeleteFile struct {
		Message struct {
			Message string `graphql:"message"`
		} `graphql:"... on Message"`
	} `graphql:"input:{path: $path}"`
}

// Delete - deletes file from the file storage.
func (f fileStorageApi) Delete(ctx context.Context, token string, path string) error {
	httpClient := oauth2.NewClient(context.Background(), nil)
	httpClient.Timeout = time.Second * time.Duration(f.cfg.Timeout)
	client := graphql.NewClient(f.cfg.URL, httpClient)
	var rm = graphql.RequestModifier(func(r *http.Request) {
		r.Header.Add("Authorization", "Bearer "+token)
	})
	client = client.WithRequestModifier(rm)
	variables := map[string]interface{}{"path": path}
	err := client.Mutate(ctx, &dfs, variables)
	if err != nil {
		return fmt.Errorf("i can't send graphQL query to file storage service: %w", err)
	}

	return nil
}

func (f fileStorageApi) GetFiles(ctx context.Context, folder string) error {
	//TODO implement me
	panic("implement me")
}

func (f fileStorageApi) GetPublicFile(ctx context.Context, path string) error {
	//TODO implement me
	panic("implement me")
}
