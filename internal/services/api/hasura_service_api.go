// Package api this file implements API functions for collaboration with external Hasura shop.
// For more information go to link: https://tglobaldev.atlassian.net/wiki/spaces/BACK/pages/17137665/Hasura+market+usage
package api

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/hasura/go-graphql-client"
	"gitlab.com/tglobal/file-parser-service/config"
	"gitlab.com/tglobal/file-parser-service/internal/models"
	interfaces2 "gitlab.com/tglobal/file-parser-service/internal/services/interfaces"
	"golang.org/x/oauth2"
	"io"
	"net/http"
	"time"
)

type SenderServiceApiInterface interface {
	SendProducts(ctx context.Context, fileID int, request string) error
}

type hasuraServiceApi struct {
	cfg config.SenderService
	j   interfaces2.JournalService
}

func NewHasuraServiceApi(cfg config.SenderService, jService interfaces2.JournalService) SenderServiceApiInterface {
	return &hasuraServiceApi{cfg, jService}
}

// TODO use correct name.
var m struct {
	Insert_catalog_item struct {
		Affected_rows int `graphql:"affected_rows"`
		//returning struct {
		//	uuid string
		//}
	} `graphql:"insert_catalog_item(objects: $objects)"`
}

// SendProducts sends products to Hasura shop.
func (h hasuraServiceApi) SendProducts(ctx context.Context, fileID int, request string) error {
	httpClient := oauth2.NewClient(context.Background(), nil)
	httpClient.Timeout = time.Second * time.Duration(h.cfg.Timeout)
	client := graphql.NewClient(h.cfg.URL, httpClient)
	var rm = graphql.RequestModifier(func(r *http.Request) {
		r.Header.Add("x-hasura-admin-secret", h.cfg.Token)
		r.Header.Add("Hasura-Client-Name", "hasura-console")

		reader := bytes.NewReader([]byte(request))
		r.Body = io.NopCloser(reader)
		r.ContentLength = int64(len(request))
	})
	client = client.WithRequestModifier(rm)

	h.saveRequestToJournal(fileID, request)

	fakeRequest := "mutation MyMutation($objects: [catalog_item_insert_input!] = {}){insert_catalog_item(objects: $objects) {affected_rows}}"
	if err := client.Exec(ctx, fakeRequest, &m, nil); err != nil {
		h.saveErrorResponseToJournal(fileID, err.Error())
		return fmt.Errorf("i can't send mutation to Hasura: %w", err)
	} else {
		h.saveSuccessResponseToJournal(fileID, m)
	}

	return nil
}

func (h hasuraServiceApi) saveRequestToJournal(fileID int, request string) {
	var bjModel = models.Journal{
		FileID:      fileID,
		Title:       models.SendToHasuraRequest,
		Description: request,
		URL:         h.cfg.URL,
	}
	_, err := h.j.Save(bjModel)
	if err != nil {
		fmt.Println("i can't write record to the journal: %w", err)
	}
}

func (h hasuraServiceApi) saveSuccessResponseToJournal(fileID int, m interface{}) {
	jsonRes, err := json.Marshal(m)
	if err != nil {
		fmt.Println("i can't convert variables to json: %w", err)
	}
	var ajModel = models.Journal{
		FileID:          fileID,
		Title:           models.SendToHasuraResponse,
		URL:             h.cfg.URL,
		ResponseStatus:  200,
		ResponseContent: string(jsonRes),
	}
	_, err = h.j.Save(ajModel)
	if err != nil {
		fmt.Println("i can't write record to the journal: %w", err)
	}
}

func (h hasuraServiceApi) saveErrorResponseToJournal(fileID int, errMessage string) {
	var ajModel = models.Journal{
		FileID:          fileID,
		Title:           models.SendToHasuraResponse,
		URL:             h.cfg.URL,
		ResponseStatus:  204,
		ResponseContent: errMessage,
	}
	_, err := h.j.Save(ajModel)
	if err != nil {
		fmt.Println("i can't write record to the journal: %w", err)
	}
}
