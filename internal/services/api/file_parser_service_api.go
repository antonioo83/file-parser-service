// Package api this file is used to test the current project.
// For more information go to link: https://tglobaldev.atlassian.net/wiki/spaces/BACK/pages/25231361/File+parser
package api

import (
	"bytes"
	"context"
	"github.com/hasura/go-graphql-client"
	"golang.org/x/oauth2"
	"io"
	"net/http"
	"time"
)

type FileParserServiceApiInterface interface {
	ParseFile(ctx context.Context, req ParseFileRequest) (content []byte, err error)
	CreateFileUserStructure(ctx context.Context, request string) (content []byte, err error)
}

type fileParserServiceApi struct {
	url     string
	token   string
	timeout int64
}

func NewFileParserServiceApi(url string, token string, timeout int64) FileParserServiceApiInterface {
	return &fileParserServiceApi{url, token, timeout}
}

type ParseFileRequest struct {
	Files []FileItem `json:"files"`
}

type FileItem struct {
	FileUid string `json:"fileUid"`
	Path    string `json:"path"`
	Type    string `json:"type"`
}

// ParseFile - sends request for parsing header of a file.
func (f fileParserServiceApi) ParseFile(ctx context.Context, req ParseFileRequest) (content []byte, err error) {
	httpClient := oauth2.NewClient(context.Background(), nil)
	httpClient.Timeout = time.Second * time.Duration(f.timeout)
	client := graphql.NewClient(f.url, httpClient)
	var rm = graphql.RequestModifier(func(r *http.Request) {
		r.Header.Add("Authorization", "Bearer "+f.token)
	})
	client = client.WithRequestModifier(rm)
	var variables = map[string]interface{}{"files": req.Files}
	query := `mutation newFile($files:[FileItem!]!) {
		  newFile(files: $files) {
			filename
			type
			columns {
			  label
			  type
			  position
			  value
			}
		  }
		}`
	return client.ExecRaw(ctx, query, variables)
}

// CreateFileUserStructure - sends request with additional column attributes for send products to Hasura.
func (f fileParserServiceApi) CreateFileUserStructure(ctx context.Context, request string) (content []byte, err error) {
	httpClient := oauth2.NewClient(context.Background(), nil)
	httpClient.Timeout = time.Second * time.Duration(f.timeout)
	client := graphql.NewClient(f.url, httpClient)
	var rm = graphql.RequestModifier(func(r *http.Request) {
		r.Header.Add("Authorization", "Bearer "+f.token)
		reader := bytes.NewReader([]byte(request))
		r.Body = io.NopCloser(reader)
		r.ContentLength = int64(len(request))
	})
	client = client.WithRequestModifier(rm)

	return client.ExecRaw(ctx, "", nil)
}
