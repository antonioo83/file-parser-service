// Package api this file implements API functions for collaboration with external Authorization Service.
// For more information go to link: https://tglobaldev.atlassian.net/wiki/spaces/BACK/pages/15761460/Auth+usage
package api

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/hasura/go-graphql-client"
	"gitlab.com/tglobal/file-parser-service/config"
	"gitlab.com/tglobal/file-parser-service/internal/models"
	interfaces2 "gitlab.com/tglobal/file-parser-service/internal/services/interfaces"
	"golang.org/x/oauth2"
	"time"
)

type AccessTokenRequest struct {
	Login    string
	Password string
}

type AuthServiceApiInterface interface {
	LoginUser(ctx context.Context, request AccessTokenRequest) (string, error)
}

type authServiceApi struct {
	cfg config.Auth
	j   interfaces2.JournalService
}

func NewAuthServiceApi(cfg config.Auth, jService interfaces2.JournalService) AuthServiceApiInterface {
	return &authServiceApi{cfg, jService}
}

var lus struct {
	LoginUser struct {
		AccessToken  string
		RefreshToken string
	} `graphql:"loginUser(email: $email, password: $password)"`
}

// LoginUser - creates user and return JWT access token.
func (a authServiceApi) LoginUser(ctx context.Context, request AccessTokenRequest) (string, error) {
	httpClient := oauth2.NewClient(ctx, nil)
	httpClient.Timeout = time.Second * time.Duration(a.cfg.Timeout)
	client := graphql.NewClient(a.cfg.URL, httpClient)
	variables := map[string]interface{}{
		"email":    request.Login,
		"password": request.Password,
	}
	a.saveRequestToJournal(nil)
	if err := client.Mutate(ctx, &lus, variables); err != nil {
		a.saveErrorResponseToJournal(err.Error())
		return "", fmt.Errorf("i can't send mutation to Hasura: %w", err)
	} else {
		a.saveSuccessResponseToJournal(m)
	}

	return lus.LoginUser.AccessToken, nil
}

func (a authServiceApi) saveRequestToJournal(variables map[string]interface{}) {
	jsonReq, err := json.Marshal(variables)
	if err != nil {
		fmt.Println("i can't convert variables to json: %w", err)
	}
	var bjModel = models.Journal{
		Title:       models.GetTokenRequest,
		Description: string(jsonReq),
		URL:         a.cfg.URL,
	}
	_, err = a.j.Save(bjModel)
	if err != nil {
		fmt.Println("i can't write record to the journal: %w", err)
	}
}

func (a authServiceApi) saveSuccessResponseToJournal(m interface{}) {
	jsonRes, err := json.Marshal(m)
	if err != nil {
		fmt.Println("i can't convert variables to json: %w", err)
	}
	var ajModel = models.Journal{
		Title:           models.GetTokenResponse,
		URL:             a.cfg.URL,
		ResponseStatus:  200,
		ResponseContent: string(jsonRes),
	}
	_, err = a.j.Save(ajModel)
	if err != nil {
		fmt.Println("i can't write record to the journal: %w", err)
	}
}

func (a authServiceApi) saveErrorResponseToJournal(errMessage string) {
	var ajModel = models.Journal{
		Title:           models.GetTokenResponse,
		URL:             a.cfg.URL,
		ResponseStatus:  204,
		ResponseContent: errMessage,
	}
	_, err := a.j.Save(ajModel)
	if err != nil {
		fmt.Println("i can't write record to the journal: %w", err)
	}
}
