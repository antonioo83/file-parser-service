// Package services service journaling. At the current moment write data to a database.
package services

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/tglobal/file-parser-service/graph/model"
	"gitlab.com/tglobal/file-parser-service/internal/models"
	"gitlab.com/tglobal/file-parser-service/internal/repositories/interfaces"
	"gitlab.com/tglobal/file-parser-service/internal/services/interfaces"
)

type journalService struct {
	context context.Context
	writer  interfaces.JournalRepository
}

func NewJournalService(context context.Context, writer interfaces.JournalRepository) interfaces2.JournalService {
	return &journalService{context, writer}
}

// Save - creates journal record to the database.
func (j journalService) Save(journal models.Journal) (int, error) {
	return j.writer.Save(journal)
}

func (j journalService) SaveFileParseRequestToJournal(jModel models.Journal, files []*model.FileItem) {
	jsonReq, err := json.Marshal(files)
	if err != nil {
		fmt.Println("i can't convert variables to json: %w", err)
	}
	jModel.Description = string(jsonReq)
	_, err = j.Save(jModel)
	if err != nil {
		fmt.Println("i can't write record to the journal: %w", err)
	}
}

func (j journalService) SaveUserFileStructRequestToJournal(jModel models.Journal, fs []*model.FileStructInput) {
	jsonReq, err := json.Marshal(fs)
	if err != nil {
		fmt.Println("i can't convert variables to json: %w", err)
	}
	jModel.Description = string(jsonReq)
	_, err = j.Save(jModel)
	if err != nil {
		fmt.Println("i can't write record to the journal: %w", err)
	}
}

func (j journalService) SaveRequestToJournal(jModel models.Journal, variables map[string]interface{}) {
	jsonReq, err := json.Marshal(variables)
	if err != nil {
		fmt.Println("i can't convert variables to json: %w", err)
	}
	jModel.Description = string(jsonReq)
	_, err = j.Save(jModel)
	if err != nil {
		fmt.Println("i can't write record to the journal: %w", err)
	}
}

func (j journalService) SaveResponseToJournal(jModel models.Journal, m interface{}) {
	jsonRes, err := json.Marshal(m)
	if err != nil {
		fmt.Println("i can't convert variables to json: %w", err)
	}
	jModel.ResponseContent = string(jsonRes)
	_, err = j.Save(jModel)
	if err != nil {
		fmt.Println("i can't write record to the journal: %w", err)
	}
}

func (j journalService) SaveErrorResponseToJournal(jModel models.Journal, errMessage string) {
	jModel.ResponseContent = errMessage
	_, err := j.Save(jModel)
	if err != nil {
		fmt.Println("i can't write record to the journal: %w", err)
	}
}
