// Package services - this service allows to use template file for generate source request to send the Hasura shop.
package services

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/tglobal/file-parser-service/config"
	"os"
	"reflect"
	"regexp"
	"strings"
	"text/template"
)

type TemplateInterface interface {
	GetRequest(templateName string, fileColumns map[int]*FileColumnResponse) (query string, err error)
}

type templateService struct {
	configs []config.HasuraTemplate
}

func NewTemplateService(requests []config.HasuraTemplate) TemplateInterface {
	return &templateService{requests}
}

type FileRows struct {
	ParamName   string
	ParamValues []string
}

// queryIndex - block query in the template file.
const queryIndex = "query"

// variableIndex - block variable in the template file.
const variableIndex = "variables"

// templatePath - directory to the template files.
const templatePath = "templates/"

// GetRequest - gets generated GraphQL request for Hasura shop.
// 1. Loads parameters from config.json and encode values to json format;
// 2. Loads template from "/tmp" catalogs with use config parameters;
// 3. Compress template with removed "\t", "\r", "\n", "\s" chars;
// 4. Generate request: {"query":"encoded json string","variables": encodedJsonObject}
func (t templateService) GetRequest(templateName string, fileColumns map[int]*FileColumnResponse) (query string, err error) {
	cfg, err := t.getTemplateConfig(templateName)
	if err != nil {
		return "", fmt.Errorf("template not found: %v", err)
	}

	basePath, err := os.Getwd()
	if err != nil {
		return "", fmt.Errorf("i can't get work directory: %v", err)
	}
	tempPath := basePath + "/" + templatePath + templateName
	content, err := os.ReadFile(tempPath)
	if err != nil {
		return "", fmt.Errorf("i can't read the file '%s because got error : %v", tempPath, err)
	}

	var re = regexp.MustCompile(`\s{2}|\r|\n|\t`)
	clearedContent := re.ReplaceAllString(string(content), "")

	var fns = template.FuncMap{
		"notLast": func(x int, a interface{}) bool {
			return x != reflect.ValueOf(a).Len()-1
		},
	}
	tmpl, err := template.New(variableIndex).Funcs(fns).Parse(clearedContent)
	if err != nil {
		return "", fmt.Errorf("template parsed error: %v", err)
	}

	var tempVarBuff bytes.Buffer
	tempVarWriter := bufio.NewWriter(&tempVarBuff)
	params := *t.getInitializedParams(fileColumns)
	params = *t.loadSystemParams(fileColumns, params)
	params = *t.loadStaticParams(*cfg, fileColumns, params)
	params = *t.loadParams(*cfg, fileColumns, params)
	err = tmpl.ExecuteTemplate(tempVarWriter, variableIndex, params)
	if err != nil {
		return "", fmt.Errorf("template '%s' executed error: %v", variableIndex, err)
	}
	err = tempVarWriter.Flush()
	if err != nil {
		return "", fmt.Errorf("template '%s' flush error: %v", variableIndex, err)
	}

	var tempQueryBuff bytes.Buffer
	tempQueryWriter := bufio.NewWriter(&tempQueryBuff)
	err = tmpl.ExecuteTemplate(tempQueryWriter, queryIndex, params)
	if err != nil {
		return "", fmt.Errorf("template '%s' executed error: %v", queryIndex, err)
	}
	err = tempQueryWriter.Flush()
	if err != nil {
		return "", fmt.Errorf("template '%s' flush error: %v", queryIndex, err)
	}

	query = fmt.Sprintf("{\"query\":\"%s\",\"variables\":%s}", tempQueryBuff.String(), tempVarBuff.String())

	return query, nil
}

// getTemplateConfig - gets template config from config.json.
func (t templateService) getTemplateConfig(templateName string) (*config.HasuraTemplate, error) {
	for _, cfg := range t.configs {
		if cfg.Template == templateName {
			return &cfg, nil
		}
	}

	return nil, fmt.Errorf("template '%s' wasn't found", templateName)
}

// getInitializedParams - gets initialized parameters map.
func (t templateService) getInitializedParams(fileColumns map[int]*FileColumnResponse) *map[int]map[string]interface{} {
	var params = make(map[int]map[string]interface{})
	for _, fc := range fileColumns {
		for i := 0; i < len(fc.Rows); i++ {
			params[i] = make(map[string]interface{})
		}
		break
	}

	return &params
}

// loadSystemParams - gets system parameters. System parameters are parameters that are forced.
func (t templateService) loadSystemParams(fileColumns map[int]*FileColumnResponse, params map[int]map[string]interface{}) *map[int]map[string]interface{} {
	for _, col := range fileColumns {
		for index := range col.Rows {
			params[index]["companySellerUUID"] = col.CompanySellerUUID
			params[index]["catalogCategoryKey"] = col.CatalogCategoryKey
		}
	}

	return &params
}

// loadStaticParams - gets parameters from config. Static parameters are parameters that have static value wrote in the config.
func (t templateService) loadStaticParams(cfg config.HasuraTemplate, fileColumns map[int]*FileColumnResponse, params map[int]map[string]interface{}) *map[int]map[string]interface{} {
	for _, col := range fileColumns {
		for index := range col.Rows {
			for _, sp := range cfg.StaticParams {
				params[index][sp.ParamName] = sp.ParamValue
			}
		}
	}

	return &params
}

// loadParams - gets parameters from config.
func (t templateService) loadParams(cfg config.HasuraTemplate, fileColumns map[int]*FileColumnResponse, params map[int]map[string]interface{}) *map[int]map[string]interface{} {
	for _, col := range fileColumns {
		for index, row := range col.Rows {
			for _, cfgParam := range cfg.Params {
				if cfgParam.Column == row.Key {
					valueJson, _ := json.Marshal(row.Value)
					value := strings.Trim(string(valueJson), "\"")
					params[index][cfgParam.ParamName] = value
					if cfgParam.UIDName != "" {
						params[index][cfgParam.UIDName] = row.UUID
					}
				}
			}
		}
	}

	return &params
}
