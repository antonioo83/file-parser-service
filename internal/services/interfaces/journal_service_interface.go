package interfaces2

import (
	"gitlab.com/tglobal/file-parser-service/graph/model"
	"gitlab.com/tglobal/file-parser-service/internal/models"
)

type JournalService interface {
	// Save create journal record.
	Save(journal models.Journal) (int, error)
	SaveRequestToJournal(jModel models.Journal, variables map[string]interface{})
	SaveFileParseRequestToJournal(jModel models.Journal, files []*model.FileItem)
	SaveUserFileStructRequestToJournal(jModel models.Journal, fs []*model.FileStructInput)
	SaveResponseToJournal(jModel models.Journal, m interface{})
	SaveErrorResponseToJournal(jModel models.Journal, errMessage string)
}
