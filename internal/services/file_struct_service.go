package services

import (
	"fmt"
	"github.com/jackc/pgx/v4/pgxpool"
	"github.com/xuri/excelize/v2"
	"gitlab.com/tglobal/file-parser-service/config"
	"gitlab.com/tglobal/file-parser-service/internal/repositories/interfaces"
	"gitlab.com/tglobal/file-parser-service/internal/repositories/postgres"
	interfaces2 "gitlab.com/tglobal/file-parser-service/internal/services/interfaces"
	"path/filepath"
	"strconv"
	"strings"
)

type FileRequest struct {
	// FileUID - file UID.
	FileUID string `json:"fileUid"`
	// Path - filepath to local storage.
	Path string `json:"path"`
	// Type - extension of a file.
	Type string `json:"type"`
}

type FileStructResponse struct {
	// FileUID - file UID.
	FileUID string `json:"fileUid"`
	// Filename - name of the file.
	Filename string `json:"filename"`
	// Type - data type of column (integer, string etc): column.type.
	Type string `json:"type"`
	// IsError - one of values in the column has error.
	IsError bool `json:"-"`
	// Column request array.
	Columns []ColumnsResponse
}

type ColumnsResponse struct {
	// Label - label column of a document: column.label;
	Label string `json:"label"`
	// Type - data type of column (integer, string etc): column.type.
	Type string `json:"type"`
	// Position - number of column (counting starts from one): column.position.
	Position int `json:"position"`
	// Value - value cell of a document.
	Value string `json:"value"`
	// Errors - contains information about error in the column.
	Errors []ColumnErrorResponse
}

type ColumnErrorResponse struct {
	//RowPosition - row number (counting starts from one).
	RowPosition int `json:"rowPosition"`
	//ColumnPosition - column number (counting starts from zero).
	ColumnPosition int `json:"columnPosition"`
	// ErrorCode - error code.
	ErrorCode int `json:"errorCode"`
	// ErrorMessage - error message.
	ErrorMessage string `json:"errorMessage"`
}

type FileStructRequest struct {
	// FileUID - file UID.
	FileUID string `json:"fileUid"`
	// Filename - name of the file.
	Filename string `json:"filename"`
	// Type - data type of column (integer, string etc): column.type.
	Type string `json:"type"`
	// Column request array.
	Columns []ColumnRequest
}

type ColumnRequest struct {
	// Label - label column of a document: column.label;
	Label string `json:"label"`
	// Type - data type of column (integer, string etc): column.type.
	Type string `json:"type"`
	// Position - number of column (counting starts from one): column.position.
	Position int `json:"position"`
	// Value - value cell of a document.
	Value string `json:"value"`
}

type UserFileStructRequest struct {
	// FileUID - file UID.
	FileUID string `json:"fileUid"`
	// CompanyUID - UID of the company in the Hasura shop.
	CompanyUID string `json:"companyUID"`
	// Filename - name of the file.
	Filename string `json:"filename"`
	// Type - extension of a file.
	Type string `json:"type"`
	// Template - name of a template for generate request to Hasura shop.
	Template string `json:"template"`
	// Column request array.
	Columns []ColumnUserRequest
}

type ColumnUserRequest struct {
	// Code - column id (request: column.uuid) (duplicates "PropertyUID").
	Code string `json:"code" copier:"UUID"`
	// PropertyKey - property key of a product: column.propertyKey.
	PropertyKey string `json:"propertyKey"`
	// CatalogCategoryKey - catalog of a product: column.catalogCategoryKey.
	CatalogCategoryKey string `json:"catalogCategoryKey"`
	// Label - label column of a document: column.label;
	Label string `json:"label"`
	// Type - data type of column (integer, string etc): column.type.
	Type string `json:"type"`
	// Position - number of column (counting starts from one): column.position.
	Position int `json:"position"`
	// Value - value cell of a document: column.value
	Value string `json:"value"`
	// Description - described product: column.description.
	Description string `json:"description"`
}

type FileStructServiceInterface interface {
	GetFileStructures(filePath string) (*FileStructResponse, error)
	GetFileColumns(message SendFileProductRequest) (map[int]*FileColumnResponse, error)
	GetFileRowCount(filepath string) (int, error)
}

type fileStructService struct {
	cfg        config.FileStructValidator
	connection *pgxpool.Pool
	fStructRep interfaces.FileStructRepository
	jService   interfaces2.JournalService
}

func NewFileStructService(cfg config.FileStructValidator, connection *pgxpool.Pool, fStructRep interfaces.FileStructRepository,
	jService interfaces2.JournalService) FileStructServiceInterface {
	return &fileStructService{cfg, connection, fStructRep, jService}
}

// GetFileStructures - gets info about of document headers.
func (f fileStructService) GetFileStructures(filePath string) (*FileStructResponse, error) {
	var response FileStructResponse
	response.FileUID = ""
	response.Filename = filepath.Base(filePath)
	response.Type = filepath.Ext(filePath)
	file, err := excelize.OpenFile(filePath)
	if err != nil {
		return nil, fmt.Errorf("i can't save file by filepath %s: %w", filePath, err)
	}
	defer func() {
		if fErr := file.Close(); fErr != nil {
			fmt.Printf("i can't close the file by filepath %s: %v\n", filePath, fErr)
		}
	}()

	var colResponses []ColumnsResponse
	sheetName := file.GetSheetName(0)
	rows, err := file.GetRows(sheetName)
	if f.validateHeaderRow(rows) != nil {
		return nil, fmt.Errorf("document has wrong format: %v", err)
	}

	for index, row := range rows[0] {
		index++
		var colResponse ColumnsResponse
		colResponse.Value = row
		colResponse.Label = f.getColumnName(index)
		colResponse.Position = index
		ct, _ := file.GetCellType(sheetName, colResponse.Label+strconv.Itoa(index))
		colResponse.Type = f.getCellTypeAsString(ct)
		colResponses = append(colResponses, colResponse)
	}
	response.Columns = colResponses
	resp := f.addErrorCells(rows, response)

	return &resp, nil
}

// validateHeaderRow - document have to contain required column names check this.
func (f fileStructService) validateHeaderRow(cols [][]string) error {
	for _, reqName := range f.cfg.RequiredColumns {
		for _, name := range cols[0] {
			if strings.Trim(name, " ") == reqName {
				return nil
			}
		}
	}

	//TODO this message contains hard code: "units".
	return fmt.Errorf("document have to contain \"units\" column")
}

// addErrorCells - added error messages for every cells of column.
func (f fileStructService) addErrorCells(rows [][]string, response FileStructResponse) FileStructResponse {
	for colIndex, col := range response.Columns {
		for rowIndex, rowValues := range rows {
			if rowIndex == 0 {
				continue
			}
			colErr := f.validateRowValue(col.Value, rowIndex, col.Position, rowValues[col.Position-1])
			if colErr != nil {
				response.IsError = true
				response.Columns[colIndex].Errors = append(response.Columns[colIndex].Errors, *colErr)
			}
		}
	}

	return response
}

const WrongUnitError = 10

func (f fileStructService) validateRowValue(columnName string, rowPos int, colPos int, rowValue string) *ColumnErrorResponse {
	for _, colVal := range f.cfg.Columns {
		if strings.Trim(columnName, " ") == colVal.Name {
			for _, allowValue := range colVal.AllowValues {
				if rowValue == allowValue {
					return nil
				}
			}
			response := ColumnErrorResponse{
				RowPosition:    rowPos,
				ColumnPosition: colPos,
				ErrorCode:      WrongUnitError,
				ErrorMessage:   "you should using only next units: " + strings.Join(colVal.AllowValues, ","),
			}
			return &response
		}
	}

	return nil
}

// getColumnName prints Excel column name for a given column number
func (f fileStructService) getColumnName(columnNumber int) string {
	var columnName = ""
	for ok := true; ok; ok = columnNumber > 0 {
		rem := columnNumber % 26
		// If remainder is 0, then a
		// 'Z' must be there in output
		if rem == 0 {
			columnName += "Z"
			columnNumber = (columnNumber / 26) - 1
		} else {
			// If remainder is non-zero
			columnName += string((rem - 1) + int('A'))
			columnNumber = columnNumber / 26
		}
	}

	return f.reverse(columnName)
}

// reverse - revers labels.
func (f fileStructService) reverse(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

type SendFileProductRequest struct {
	FileID       int
	FilePath     string
	Template     string
	StartNumber  int
	FinishNumber int
}

type FileColumnResponse struct {
	CompanySellerUUID  string `json:"CompanySellerUUID"`
	CatalogCategoryKey string `json:"CatalogCategoryKey"`
	Position           int    `json:"Position"`
	Description        string `json:"Description"`
	Rows               []RowResponse
}

type RowResponse struct {
	Label       string `json:"Label"`
	Type        string `json:"Type"`
	Position    int    `json:"Position"`
	Value       string `json:"Value"`
	UUID        string `json:"UUID"`
	Key         string `json:"Key"`
	Description string `json:"Description"`
}

// GetFileRowCount - gets count rows in the document.
func (f fileStructService) GetFileRowCount(filepath string) (int, error) {
	file, err := excelize.OpenFile(filepath)
	if err != nil {
		return 0, fmt.Errorf("i can't save file by filepath %s: %w", filepath, err)
	}
	defer func() {
		if fErr := file.Close(); fErr != nil {
			fmt.Printf("i can't close the file by filepath %s: %v\n", filepath, fErr)
		}
	}()

	sheetName := file.GetSheetName(0)
	rows, err := file.GetRows(sheetName)

	return len(rows), nil
}

// GetFileColumns - gets count rows in the document.
func (f fileStructService) GetFileColumns(message SendFileProductRequest) (map[int]*FileColumnResponse, error) {
	file, err := excelize.OpenFile(message.FilePath)
	if err != nil {
		return nil, fmt.Errorf("i can't save file by filepath %s: %w", message.FilePath, err)
	}
	defer func() {
		if fErr := file.Close(); fErr != nil {
			fmt.Printf("i can't close the file by filepath %s: %v\n", message.FilePath, fErr)
		}
	}()

	models, err := f.fStructRep.FindALLByFileID(message.FileID, postgres.PosKey)
	if err != nil {
		return nil, fmt.Errorf("i can't get an user file struct: %w", err)
	}

	var fcResponses = make(map[int]*FileColumnResponse)
	for pos, model := range *models {
		fcResponses[pos] = &FileColumnResponse{
			Position:           model.Position,
			CompanySellerUUID:  model.FileRelation.CompanyUID,
			CatalogCategoryKey: model.CatalogCategoryKey,
			Description:        model.FileRelation.Description,
			Rows:               []RowResponse{},
		}
	}

	sheetName := file.GetSheetName(0)
	rows, err := file.GetRows(sheetName)
	for index, row := range rows {
		if index == 0 || index < message.StartNumber {
			continue
		}
		if index > message.FinishNumber {
			break
		}
		for cIndex, cellValue := range row {
			cIndex++
			model, ok := (*models)[cIndex]
			if !ok {
				return nil, fmt.Errorf("i can't get user struct for a file column: %d", cIndex)
			}
			var colResponse = RowResponse{}
			colResponse.UUID = model.PropertyUID
			colResponse.Key = model.PropertyKey
			colResponse.Value = cellValue
			colResponse.Label = f.getColumnName(cIndex)
			colResponse.Position = cIndex
			ct, _ := file.GetCellType(sheetName, colResponse.Label+strconv.Itoa(cIndex))
			colResponse.Type = f.getCellTypeAsString(ct)
			fcResponses[cIndex].Rows = append(fcResponses[cIndex].Rows, colResponse)
		}
	}

	return fcResponses, nil
}

// GetFileColumns - defines data type of a column.
func (f fileStructService) getCellTypeAsString(ct excelize.CellType) string {
	cellType := "unknown"
	if ct == excelize.CellTypeBool {
		cellType = "boolean"
	} else if ct == excelize.CellTypeDate {
		cellType = "date"
	} else if ct == excelize.CellTypeNumber {
		cellType = "number"
	} else if ct == excelize.CellTypeString {
		cellType = "string"
	} else if ct == excelize.CellTypeUnset {
		cellType = "unset"
	}

	return cellType
}
