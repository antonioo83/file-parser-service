package graph

import (
	"context"
	"fmt"
	"github.com/99designs/gqlgen/graphql"
	"github.com/jinzhu/copier"
	"gitlab.com/tglobal/file-parser-service/graph/generated"
	"gitlab.com/tglobal/file-parser-service/graph/model"
	"gitlab.com/tglobal/file-parser-service/internal/models"
	"gitlab.com/tglobal/file-parser-service/internal/services"
	interfaces2 "gitlab.com/tglobal/file-parser-service/internal/services/interfaces"
)

type Resolver struct {
	FileService services.FileInterface
	JService    interfaces2.JournalService
}

func NewRootResolvers(fs services.FileInterface, js interfaces2.JournalService) generated.Config {
	cfg := generated.Config{
		Resolvers: &Resolver{
			FileService: fs,
			JService:    js,
		},
	}
	cfg.Directives.IsAuthenticated = func(ctx context.Context, obj interface{}, resolver graphql.Resolver) (res interface{}, err error) {
		ctxUserID := ctx.Value(services.AuthUserCtxKey)
		if ctxUserID != nil {
			return resolver(ctx)
		} else {
			return nil, fmt.Errorf("authentication error")
		}
	}
	return cfg
}

func (r *mutationResolver) parseNewFile(ctx context.Context, files []*model.FileItem) (model.FileStructResult, error) {
	var jModel = models.Journal{
		Title: models.GetFileStructRequest,
		URL:   "localhost",
		Token: "",
	}
	r.JService.SaveFileParseRequestToJournal(jModel, files)
	jModel.Title = models.GetFileStructResponse

	requests, err := r.convertGraphModelsToRequests(files)
	if err != nil {
		r.JService.SaveErrorResponseToJournal(jModel, err.Error())
		return nil, fmt.Errorf("can't convert request: %w", err)
	}
	if len(requests) > 1 {
		r.JService.SaveErrorResponseToJournal(jModel, err.Error())
		return nil, fmt.Errorf("i can't process request with more than one item: %w", err)
	}
	fsResponse, err := r.FileService.ParseFile(ctx, requests[0])
	if err != nil {
		r.JService.SaveErrorResponseToJournal(jModel, err.Error())
		return nil, fmt.Errorf("can't parse request: %w", err)
	}

	var fsResponses []*services.FileStructResponse
	fsResponses = append(fsResponses, fsResponse)
	gResponse, err := r.convertResponsesToGraphResponse(fsResponses)
	if err != nil {
		r.JService.SaveErrorResponseToJournal(jModel, err.Error())
		return nil, fmt.Errorf("can't convert response: %w", err)
	}

	r.JService.SaveResponseToJournal(jModel, gResponse)

	return gResponse, nil
}

func (r *mutationResolver) convertGraphModelsToRequests(files []*model.FileItem) ([]services.FileRequest, error) {
	var requests []services.FileRequest
	for _, item := range files {
		var request = services.FileRequest{}
		err := copier.Copy(&request, &item)
		if err != nil {
			return nil, fmt.Errorf("can't copied data for manufacturing:%w", err)
		}
		requests = append(requests, request)
	}

	return requests, nil
}

func (r *mutationResolver) convertResponsesToGraphResponse(fsResponses []*services.FileStructResponse) (model.FileStructResult, error) {
	var responses []*model.FileStruct
	var errResponse model.FileStructErrors
	for _, fsResponse := range fsResponses {
		var response = model.FileStruct{}
		err := copier.Copy(&response, &fsResponse)
		if err != nil {
			return nil, fmt.Errorf("can't copy response to graph response: %w", err)
		}
		responses = append(responses, &response)
		for _, col := range fsResponse.Columns {
			for _, colErr := range col.Errors {
				var fsError = model.FileStructError{}
				err := copier.Copy(&fsError, &colErr)
				if err != nil {
					return nil, fmt.Errorf("can't copy response to graph response: %w", err)
				}
				errResponse.Items = append(errResponse.Items, &fsError)
			}
		}
	}

	if len(errResponse.Items) > 0 {
		return errResponse, nil
	}

	return responses[0], nil
}

func (r *mutationResolver) createUserFileStruct(ctx context.Context, fs []*model.FileStructInput) ([]*model.FileStructResponse, error) {
	var jModel = models.Journal{
		Title: models.CreateUserFileStructRequest,
		URL:   "localhost",
		Token: "",
	}
	r.JService.SaveUserFileStructRequestToJournal(jModel, fs)
	jModel.Title = models.CreateUserFileStructResponse

	requests, err := r.convertFsGModelsToRequests(fs)
	if err != nil {
		r.JService.SaveErrorResponseToJournal(jModel, err.Error())
		return nil, fmt.Errorf("can't convert request: %w", err)
	}
	fsResponse, err := r.FileService.CreateUserFileStruct(ctx, requests[0])
	if err != nil {
		r.JService.SaveErrorResponseToJournal(jModel, err.Error())
		return nil, fmt.Errorf("can't create user file struct:%w", err)
	}

	var fsResponses []*services.UserFileStructResponse
	fsResponses = append(fsResponses, fsResponse)
	gResponses, err := r.convertFsResponsesToGraphResponses(fsResponses)
	if err != nil {
		r.JService.SaveErrorResponseToJournal(jModel, err.Error())
		return nil, fmt.Errorf("can't convert response: %w", err)
	}

	r.JService.SaveResponseToJournal(jModel, gResponses)

	return gResponses, nil
}

func (r *mutationResolver) convertFsGModelsToRequests(fileStructs []*model.FileStructInput) ([]services.UserFileStructRequest, error) {
	var requests []services.UserFileStructRequest
	for _, item := range fileStructs {
		var request = services.UserFileStructRequest{}
		err := copier.Copy(&request, &item)
		if err != nil {
			return nil, fmt.Errorf("can't copied data from graphQL request to file struct request:%w", err)
		}
		requests = append(requests, request)
	}

	return requests, nil
}

func (r *mutationResolver) convertFsResponsesToGraphResponses(fsResponses []*services.UserFileStructResponse) ([]*model.FileStructResponse, error) {
	var responses []*model.FileStructResponse
	for _, fsResponse := range fsResponses {
		var response = model.FileStructResponse{}
		err := copier.Copy(&response, &fsResponse)
		if err != nil {
			return nil, fmt.Errorf("can't copy response to graph response: %w", err)
		}
		responses = append(responses, &response)
	}

	return responses, nil
}
