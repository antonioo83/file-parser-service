package config

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/jinzhu/copier"
	"gitlab.com/tglobal/file-parser-service/internal/utils"

	"github.com/caarlos0/env/v6"
	"os"
)

type Config struct {
	// Server address.
	ServerAddress string `env:"SERVER_ADDRESS" json:"server_address,omitempty"`
	// Connection string to the database.
	DatabaseDsn string `env:"DATABASE_DSN" json:"database_dsn,omitempty"`
	// Timeout value for a request handling of the HTTP server.
	RequestTimeoutSec int64 `env:"REQUEST_TIMEOUT_SEC"`
	// Settings of the external authorization service.
	Auth Auth `json:"auth,omitempty"`
	// Settings of the external file service.
	FileService FileService `json:"fileService,omitempty"`
	// Settings of the sender service that send products to the external Hasura shop.
	Sender SenderService `json:"senderService,omitempty"`
	// Settings of the Amazon SQS service;
	AmazonSQS AmazonSQS `json:"amazon_sqs,omitempty"`
	// Settings of the logging service;
	Logger Logger `json:"logger,omitempty"`
	// Settings of the token generator;
	TokenGen TokenGenService `json:"tokenGenService,omitempty"`
	// Settings of the template service that use for formation requests to Hasura Shop;
	HasuraTemplates []HasuraTemplate `json:"hasuraTemplates,omitempty"`
	// Settings for run tests for this project;
	Test TestService `json:"testService,omitempty"`
	// Settings of the file struct validator service use for requests validation from Frontend;
	FileStructValidator FileStructValidator `json:"fileStructValidator,omitempty"`
}

type AmazonSQS struct {
	EndpointURL string `env:"AWS_ENDPOINT" json:"endpoint_url,omitempty"`
	// Variable to the default region.
	Region string `env:"AWS_REGION" json:"region,omitempty"`
	// Profile SQS (use default) settings.
	Profile string `env:"AWS_PROFILE" json:"profile,omitempty"`
	// Credential SQS settings.
	Credential AmazonSQSCredential `json:"credential,omitempty"`
	// Receiver SQS settings.
	Receiver AmazonSQSReceiver `json:"receiver,omitempty"`
	// Sender SQS settings
	Sender AmazonSQSSender `json:"sender,omitempty"`
}

// AmazonSQSCredential These credentials use for connect to Amazon sqs.
type AmazonSQSCredential struct {
	ID     string `env:"AWS_ACCESS_KEY_ID" json:"id,omitempty"`
	Secret string `env:"AWS_SECRET_ACCESS_KEY" json:"secret,omitempty"`
	Token  string `env:"AWS_SESSION_TOKEN" json:"token,omitempty"`
}

// AmazonSQSReceiver settings for receive SQS messages.
type AmazonSQSReceiver struct {
	// The url to the queue.
	QueueUrl string `env:"AWS_QUEUE_URL" json:"queue_url,omitempty"`
	// The maximum number of messages to return.
	MaxNumberOfMessages int64 `env:"AWS_MAX_NUMBER_OF_MESSAGES" json:"max_number_of_messages,omitempty"`
	// The duration (in seconds) that the received messages are hidden from subsequent retrieve requests after being retrieved by a ReceiveMessage request.
	VisibilityTimeout int64 `env:"AWS_VISIBILITY_TIMEOUT" json:"visibility_timeout,omitempty"`
	// The duration (in seconds) for which the call waits for a message to arrive in the queue before returning.
	WaitTimeSeconds int64 `env:"AWS_WAIT_TIME_SECONDS" json:"wait_time_seconds,omitempty"`
}

// AmazonSQSSender settings for send SQS messages.
type AmazonSQSSender struct {
	// The length of time, in seconds, for which the delivery of all messages in the queue is delayed. Valid values: An integer from 0 to 900 (15 minutes).
	DelaySeconds int64 `env:"AWS_DELAY_SECONDS" json:"delay_seconds,omitempty"`
}

// Logger logging request ant response to a storage.
type Logger struct {
	// Now it service write logs to the database. You can turn off it by this property.
	IsActive bool `env:"LOGGER_ACTIVE" json:"is_active,omitempty"`
	// This property not using and reserved.
	Transport string `env:"LOGGER_TRANSPORT" json:"transport,omitempty"`
}

// Auth external service
type Auth struct {
	// URL to the external authorization service.
	URL string `env:"AUTH_SERVICE_URL" json:"url,omitempty"`
	// JWT token signed method.
	SigningMethod string
	// Path to the private key for the validation jwt token of a request (deprecated: should move to auth service).
	PrivateKeyPath string `env:"AUTH_SERVICE_PRIVATE_KEY_PATH" json:"privateKeyPath,omitempty"`
	// Path to the public key for the validation jwt token of a request and for generate JWT token for a request
	//(deprecated: should move to auth service).
	PublicKeyPath string `env:"AUTH_SERVICE_PUBLIC_KEY_PATH" json:"publicKeyPath,omitempty"`
	// Use for login to the external auth service.
	Login string `env:"AUTH_SERVICE_LOGIN" json:"login,omitempty"`
	// Use for login to the external auth service.
	Password string `env:"AUTH_SERVICE_PASSWORD" json:"password,omitempty"`
	// Timeout for every request to the auth service.
	Timeout int `env:"AUTH_SERVICE_TIMEOUT" json:"timeout,omitempty"`
}

// FileService external file storage service.
type FileService struct {
	// URL to the external file service.
	URL string `env:"FILE_SERVICE_URL" json:"url,omitempty"`
	// Timeout for every request to the auth service.
	Timeout int `env:"FILE_SERVICE_TIMEOUT" json:"timeout,omitempty"`
	// Path to the local file storage.
	TmpFilePath string `env:"FILE_SERVICE_TMP_PATH" json:"tmpFilePath,omitempty"`
}

// SenderService sends products to Hasura shop
type SenderService struct {
	// URL to the Hasura shop.
	URL string `env:"SENDER_SERVICE_URL" json:"url,omitempty"`
	// Auth token.
	Token string `env:"SENDER_SERVICE_TOKEN" json:"token,omitempty"`
	// Max product items in the one request.
	ItemCount int `env:"SENDER_SERVICE_ITEM_COUNT" json:"itemCount,omitempty"`
	// Timeout for every request to the Hasura shop.
	Timeout int `env:"SENDER_SERVICE_TIMEOUT" json:"timeout,omitempty"`
}

// TokenGenService generates JWT tokens.
type TokenGenService struct {
	// Algorithm type of the JWT token.
	Algorithm string `env:"TOKEN_GEN_SERVICE_ALG" json:"algorithm,omitempty"`
	// Path to the private key for the validation and generation JWT token of a request
	PrivateKeyPath string `env:"TOKEN_GEN_SERVICE_PRIVATE_KEY_PATH" json:"privateKeyPath,omitempty"`
	// Path to the private key for the validation and generation JWT token of a request
	PublicKeyPath string `env:"TOKEN_GEN_SERVICE_PUBLIC_KEY_PATH" json:"publicKeyPath,omitempty"`
}

// TestFile settings for testing service.
type TestFile struct {
	// Filepath to local file that will load in service.
	Filepath string `json:"filepath"`
	// Extension of this file (xlsx).
	Extension string `json:"extension"`
	// Name of this file.
	Name string `json:"name"`
	// Local file will load to the external file service. And file service have visibility option: PUBLIC or PRIVATE.
	Visibility string `json:"visibility"`
	// Local file will load to the external file service. And file service have to a folder for it.
	Folder                   string `json:"folder"`
	ParseFileResponse        string `json:"parseFileResponse"`
	CreateUserStructRequest  string `json:"createUserStructRequest"`
	CreateUserStructResponse string `json:"createUserStructResponse"`
}

// TestService can contain array of files for testing.
type TestService struct {
	Timeout int64      `json:"timeout"`
	Files   []TestFile `json:"files"`
}

// HasuraTemplate uses for formatting a request to send the Hasura shop.
type HasuraTemplate struct {
	// Template name.
	Template string `json:"template,omitempty"`
	// Array of column names that will be handling in the loaded files.
	Columns []string `json:"columns,omitempty"`
	// Array of parameters that will be using for the generation request. (Values will be extracted from loaded document).
	Params []ColumnParam `json:"params,omitempty"`
	// Array of parameters with static values that will be using for the generation request.
	StaticParams []ColumnStaticParam `json:"staticParams,omitempty"`
}

// ColumnParam uses for generating request by parameters. These values gets from user file structure request.
type ColumnParam struct {
	// The column name of the loaded document (propertyKey). This value is constant.
	Column string `json:"column,omitempty"`
	// The parameter name in the template.
	ParamName string `json:"paramName,omitempty"`
	// The name of parameter in the template extracted from "uuid" property of request (create user file structure request) (Can be empty).
	UIDName string `json:"uidName,omitempty"`
	// Description parameter. (Can be empty)
	Description string `json:"description,omitempty"`
}

// ColumnStaticParam uses for generating request by parameters with static values.
type ColumnStaticParam struct {
	// Expression for calc a mathematical expressions:5*ParamValue=? (Not used).
	Formula string `json:"formula,omitempty"`
	// The parameter name in the template.
	ParamName string `json:"paramName,omitempty"`
	// The parameter value.
	ParamValue string `json:"value,omitempty"`
	// Description parameter. (Can be empty)
	Description string `json:"description,omitempty"`
}

// FileStructValidator is validating document because document can contain only values of constant for some columns.
// for example: measure values.
type FileStructValidator struct {
	// Name of required columns loaded a document.
	RequiredColumns []string `json:"requiredColumns"`
	// Validator of a column.
	Columns []ColumnValidator `json:"columns"`
}

// ColumnValidator is validator of a column.
type ColumnValidator struct {
	// Column name.
	Name string `json:"name"`
	// Array of allow values in a column of loaded document.
	AllowValues []string `json:"allowValues"`
}

var cfg Config

// GetConfigSettings gets configuration settings of the server.
func GetConfigSettings(configFromFile *Config) (Config, error) {
	const (
		ServerAddress     = ":8080"
		DatabaseDSN       = "postgres://postgres:433370@database:5432/cert_parser"
		RequestTimeoutSec = 60
	)

	if configFromFile != nil {
		err := copier.Copy(&cfg, &configFromFile)
		if err != nil {
			return cfg, fmt.Errorf("I can't copy json config: %w", err)
		}
	}

	err := env.Parse(&cfg)
	if err != nil {
		return cfg, fmt.Errorf("I can't parse config: %w", err)
	}

	flag.StringVar(&cfg.ServerAddress, "a", cfg.ServerAddress, "The address of the local server")
	flag.StringVar(&cfg.DatabaseDsn, "d", cfg.DatabaseDsn, "Database port")
	flag.Int64Var(&cfg.RequestTimeoutSec, "rt", cfg.RequestTimeoutSec, "Timeout value for a request")

	flag.StringVar(&cfg.AmazonSQS.EndpointURL, "sqs_e_url", cfg.AmazonSQS.EndpointURL, "Endpoint URL")
	flag.StringVar(&cfg.AmazonSQS.Region, "sqs_r", cfg.AmazonSQS.Region, "Region")
	flag.StringVar(&cfg.AmazonSQS.Profile, "sqs_p", cfg.AmazonSQS.Profile, "Profile")

	flag.StringVar(&cfg.AmazonSQS.Credential.ID, "sqs_cred_id", cfg.AmazonSQS.Credential.ID, "Credential id")
	flag.StringVar(&cfg.AmazonSQS.Credential.Secret, "sqs_cred_secret", cfg.AmazonSQS.Credential.Secret, "Credential secret")
	flag.StringVar(&cfg.AmazonSQS.Credential.Token, "sqs_cred_token", cfg.AmazonSQS.Credential.Token, "Credential token")

	flag.StringVar(&cfg.AmazonSQS.Receiver.QueueUrl, "sqs_q_url", cfg.AmazonSQS.Receiver.QueueUrl, "Queue URL")
	flag.Int64Var(&cfg.AmazonSQS.Receiver.MaxNumberOfMessages, "sqs_n_mess", cfg.AmazonSQS.Receiver.MaxNumberOfMessages, "Max number of messages")
	flag.Int64Var(&cfg.AmazonSQS.Receiver.VisibilityTimeout, "sqs_vis_time", cfg.AmazonSQS.Receiver.VisibilityTimeout, "Visibility timeout")
	flag.Int64Var(&cfg.AmazonSQS.Receiver.WaitTimeSeconds, "sqs_w_time", cfg.AmazonSQS.Receiver.WaitTimeSeconds, "Wait time second")

	flag.Int64Var(&cfg.AmazonSQS.Sender.DelaySeconds, "sqs_delay_sec", cfg.AmazonSQS.Sender.DelaySeconds, "Delay seconds")

	flag.StringVar(&cfg.Sender.URL, "sdr_url", cfg.Sender.URL, "Sender URL")
	flag.StringVar(&cfg.Sender.Token, "sdr_token", cfg.Sender.Token, "Sender Token")
	flag.IntVar(&cfg.Sender.ItemCount, "sdr_item_count", cfg.Sender.ItemCount, "Sender item count of package")
	flag.IntVar(&cfg.Sender.Timeout, "sdr_timeout", cfg.Sender.Timeout, "Sender timeout of a request")

	flag.Parse()

	if cfg.ServerAddress == "" {
		cfg.ServerAddress = ServerAddress
	}
	if cfg.DatabaseDsn == "" {
		cfg.DatabaseDsn = DatabaseDSN
	}
	if cfg.RequestTimeoutSec == 0 {
		cfg.RequestTimeoutSec = RequestTimeoutSec
	}

	return cfg, nil
}

// LoadConfigFile this method read a server configurations from a file in the json format.
func LoadConfigFile(configFilePath string) (*Config, error) {
	var configFromFile Config

	file, err := os.OpenFile(configFilePath, os.O_RDONLY, 0777)
	if err != nil {
		return nil, fmt.Errorf("unable to open a configuration file: %w", err)
	}
	defer utils.ResourceClose(file)

	info, err := file.Stat()
	if err != nil {
		return nil, fmt.Errorf("unable to get statistic info about configuration file: %w", err)
	}
	filesize := info.Size()
	jsonConfig := make([]byte, filesize)

	_, err = file.Read(jsonConfig)
	if err != nil {
		return nil, fmt.Errorf("i can't read a configuration file: %w", err)
	}

	err = json.Unmarshal(jsonConfig, &configFromFile)
	if err != nil {
		return nil, fmt.Errorf("i can't parse a configuration json file: %w", err)
	}

	return &configFromFile, nil
}
