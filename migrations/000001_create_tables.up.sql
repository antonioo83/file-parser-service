--
-- PostgreSQL database dump
--

-- Dumped from database version 15.1 (Debian 15.1-1.pgdg110+1)
-- Dumped by pg_dump version 15.0

-- Started on 2023-02-03 13:16:45

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 214 (class 1259 OID 18143)
-- Name: fp_journal; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.fp_journal (
                                   id integer NOT NULL,
                                   file_id integer,
                                   title character varying(1000) DEFAULT ''::character varying NOT NULL,
                                   url character varying(1000) DEFAULT ''::character varying NOT NULL,
                                   response_status integer DEFAULT 0 NOT NULL,
                                   response_content text DEFAULT ''::text NOT NULL,
                                   created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
                                   description text DEFAULT ''::text NOT NULL
);


--
-- TOC entry 215 (class 1259 OID 18154)
-- Name: cp_journal_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.cp_journal_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3406 (class 0 OID 0)
-- Dependencies: 215
-- Name: cp_journal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.cp_journal_id_seq OWNED BY public.fp_journal.id;


--
-- TOC entry 222 (class 1259 OID 26415)
-- Name: fp_file_packages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.fp_file_packages (
                                         id integer NOT NULL,
                                         file_id integer NOT NULL,
                                         start_index integer DEFAULT 1 NOT NULL,
                                         finish_index integer DEFAULT 1 NOT NULL,
                                         status integer DEFAULT 1 NOT NULL,
                                         message character varying(500) DEFAULT ''::character varying NOT NULL,
                                         updated_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
                                         message_id character varying(128)
);


--
-- TOC entry 223 (class 1259 OID 26427)
-- Name: fp_file_packages_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.fp_file_packages_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3407 (class 0 OID 0)
-- Dependencies: 223
-- Name: fp_file_packages_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.fp_file_packages_id_seq OWNED BY public.fp_file_packages.id;


--
-- TOC entry 216 (class 1259 OID 18155)
-- Name: fp_file_structures; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.fp_file_structures (
                                           id integer NOT NULL,
                                           file_id integer NOT NULL,
                                           label character varying(200) DEFAULT ''::character varying NOT NULL,
                                           type character varying(20) DEFAULT 'string'::character varying,
                                           "position" integer DEFAULT 1 NOT NULL,
                                           value text DEFAULT ''::text NOT NULL,
                                           created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
                                           code character varying(128) DEFAULT ''::character varying NOT NULL,
                                           property_key character varying(300) DEFAULT ''::character varying NOT NULL,
                                           catalog_category_key character varying(300) DEFAULT ''::character varying NOT NULL,
                                           property_uid character varying(128) DEFAULT ''::character varying NOT NULL,
                                           description character varying(500) DEFAULT ''::character varying NOT NULL
);


--
-- TOC entry 217 (class 1259 OID 18170)
-- Name: fp_file_structures_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.fp_file_structures_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3408 (class 0 OID 0)
-- Dependencies: 217
-- Name: fp_file_structures_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.fp_file_structures_id_seq OWNED BY public.fp_file_structures.id;


--
-- TOC entry 218 (class 1259 OID 18171)
-- Name: fp_files; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.fp_files (
                                 id integer NOT NULL,
                                 user_id integer NOT NULL,
                                 code character varying(128) NOT NULL,
                                 path character varying(1000) NOT NULL,
                                 type character varying(10) NOT NULL,
                                 cache character varying(256) NOT NULL,
                                 status character varying(20) DEFAULT 'waiting'::character varying NOT NULL,
                                 message character varying(300) DEFAULT ''::character varying NOT NULL,
                                 created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
                                 updated_at timestamp without time zone,
                                 deleted_at timestamp without time zone,
                                 company_uid character varying(128) DEFAULT ''::character varying NOT NULL,
                                 description character varying(500) DEFAULT ''::character varying NOT NULL
);


--
-- TOC entry 219 (class 1259 OID 18181)
-- Name: fp_files_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.fp_files_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3409 (class 0 OID 0)
-- Dependencies: 219
-- Name: fp_files_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.fp_files_id_seq OWNED BY public.fp_files.id;


--
-- TOC entry 220 (class 1259 OID 18182)
-- Name: fp_users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.fp_users (
                                 id integer NOT NULL,
                                 token character varying(1024) NOT NULL,
                                 created_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
                                 updated_at timestamp without time zone,
                                 deleted_at timestamp without time zone,
                                 title character varying(50) DEFAULT ''::character varying NOT NULL,
                                 description character varying(100) DEFAULT ''::character varying NOT NULL,
                                 code character varying(128) DEFAULT ''::character varying NOT NULL
);


--
-- TOC entry 221 (class 1259 OID 18191)
-- Name: fp_users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.fp_users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- TOC entry 3410 (class 0 OID 0)
-- Dependencies: 221
-- Name: fp_users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.fp_users_id_seq OWNED BY public.fp_users.id;


--
-- TOC entry 3225 (class 2604 OID 26428)
-- Name: fp_file_packages id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fp_file_packages ALTER COLUMN id SET DEFAULT nextval('public.fp_file_packages_id_seq'::regclass);


--
-- TOC entry 3203 (class 2604 OID 18193)
-- Name: fp_file_structures id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fp_file_structures ALTER COLUMN id SET DEFAULT nextval('public.fp_file_structures_id_seq'::regclass);


--
-- TOC entry 3214 (class 2604 OID 18194)
-- Name: fp_files id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fp_files ALTER COLUMN id SET DEFAULT nextval('public.fp_files_id_seq'::regclass);


--
-- TOC entry 3196 (class 2604 OID 18192)
-- Name: fp_journal id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fp_journal ALTER COLUMN id SET DEFAULT nextval('public.cp_journal_id_seq'::regclass);


--
-- TOC entry 3220 (class 2604 OID 18195)
-- Name: fp_users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fp_users ALTER COLUMN id SET DEFAULT nextval('public.fp_users_id_seq'::regclass);


--
-- TOC entry 3397 (class 0 OID 26415)
-- Dependencies: 222
-- Data for Name: fp_file_packages; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3391 (class 0 OID 18155)
-- Dependencies: 216
-- Data for Name: fp_file_structures; Type: TABLE DATA; Schema: public; Owner: -
--




--
-- TOC entry 3389 (class 0 OID 18143)
-- Dependencies: 214
-- Data for Name: fp_journal; Type: TABLE DATA; Schema: public; Owner: -
--



--
-- TOC entry 3395 (class 0 OID 18182)
-- Dependencies: 220
-- Data for Name: fp_users; Type: TABLE DATA; Schema: public; Owner: -
--

INSERT INTO public.fp_users VALUES (1, 'MUIFAOr9t9GaWaLyaV9MRymejxW2jpDCq-OXYO_1UhzQG9cTkrDhzVqPpyRWyLPjIzUjjOroasK8AiGzCdeOZQofvFA5LS5ZYeuSzAfpGF7g-h8Ec_kqvd092iQCU-kX9pAYj3YXtKdUCAiB3pRfHyXIkQEZj3Rl9CguzhQcrYlI39be4LzXm7jq2LaXnAonLusBeX4Tfh9Tr0aPxpsA87XBWWdKNMQBr3HukX4GKVndeior2CfN5IvUT2lRw_u2_Es-zf2pkWF_EIIiXqEaPi1lRMUxXF7Tu93eqFaITaej5HJwxyZS', '2023-01-05 16:50:44.879192', NULL, NULL, 'janicheg@yandex.ru', '753e9524-7a32-4093-be9a-639ce0ccde2a', '753e9524-7a32-4093-be9a-639ce0ccde2a');


--
-- TOC entry 3411 (class 0 OID 0)
-- Dependencies: 215
-- Name: cp_journal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.cp_journal_id_seq', 39, true);


--
-- TOC entry 3412 (class 0 OID 0)
-- Dependencies: 223
-- Name: fp_file_packages_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.fp_file_packages_id_seq', 225, true);


--
-- TOC entry 3413 (class 0 OID 0)
-- Dependencies: 217
-- Name: fp_file_structures_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.fp_file_structures_id_seq', 579, true);


--
-- TOC entry 3414 (class 0 OID 0)
-- Dependencies: 219
-- Name: fp_files_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.fp_files_id_seq', 1, true);


--
-- TOC entry 3415 (class 0 OID 0)
-- Dependencies: 221
-- Name: fp_users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public.fp_users_id_seq', 1, true);


--
-- TOC entry 3232 (class 2606 OID 18197)
-- Name: fp_journal cp_journal_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fp_journal
    ADD CONSTRAINT cp_journal_pkey PRIMARY KEY (id);


--
-- TOC entry 3242 (class 2606 OID 26426)
-- Name: fp_file_packages fp_file_packages_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fp_file_packages
    ADD CONSTRAINT fp_file_packages_pk PRIMARY KEY (id);


--
-- TOC entry 3234 (class 2606 OID 18199)
-- Name: fp_file_structures fp_file_structures_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fp_file_structures
    ADD CONSTRAINT fp_file_structures_pk PRIMARY KEY (id);


--
-- TOC entry 3236 (class 2606 OID 18201)
-- Name: fp_files fp_files_pk; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fp_files
    ADD CONSTRAINT fp_files_pk PRIMARY KEY (id);


--
-- TOC entry 3238 (class 2606 OID 18203)
-- Name: fp_users fp_users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fp_users
    ADD CONSTRAINT fp_users_pkey PRIMARY KEY (id);


--
-- TOC entry 3240 (class 2606 OID 18205)
-- Name: fp_users fp_users_token_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fp_users
    ADD CONSTRAINT fp_users_token_key UNIQUE (token);


--
-- TOC entry 3243 (class 2606 OID 18206)
-- Name: fp_journal cp_journal_fp_files_null_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fp_journal
    ADD CONSTRAINT cp_journal_fp_files_null_fk FOREIGN KEY (file_id) REFERENCES public.fp_files(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3246 (class 2606 OID 26429)
-- Name: fp_file_packages fp_file_packages_fp_files_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fp_file_packages
    ADD CONSTRAINT fp_file_packages_fp_files_id_fk FOREIGN KEY (file_id) REFERENCES public.fp_files(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3244 (class 2606 OID 18211)
-- Name: fp_file_structures fp_file_structures_fp_files_null_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fp_file_structures
    ADD CONSTRAINT fp_file_structures_fp_files_null_fk FOREIGN KEY (file_id) REFERENCES public.fp_files(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3245 (class 2606 OID 18216)
-- Name: fp_files fp_files_fp_users_null_fk; Type: FK CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.fp_files
    ADD CONSTRAINT fp_files_fp_users_null_fk FOREIGN KEY (user_id) REFERENCES public.fp_users(id) ON UPDATE CASCADE ON DELETE CASCADE;


-- Completed on 2023-02-03 13:16:45

--
-- PostgreSQL database dump complete
--

